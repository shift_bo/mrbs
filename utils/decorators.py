# coding=utf-8
from functools import wraps

from flask import current_app
from flask import jsonify
from flask import request

from api.base import api_jsonify
from extends import redis_store
from models import Area
from models import AreaManager
from models import Role
from service.employee import EmployeeService
from utils import decrypt_token


def cache_class_property(key=None):
    """ 缓存类属性"""

    def cache_property(func):
        _cache_key = key if key else func.__name__

        @wraps(func)
        def deco(*args, **kw):
            cls = args[0]
            if hasattr(cls, _cache_key):
                return getattr(cls, _cache_key)
            else:
                result = func(*args, **kw)
                setattr(cls, _cache_key, result)
                return result

        return deco

    return cache_property


def cached(key=None, expires=600):
    """ 缓存函数
    :param store: 存储对象
    """

    def _cache_function(func):
        _cache_key = key if key else func.__name__

        @wraps(func)
        def deco(*args, **kw):
            cls = args[0]
            cache_key = cls.__name__ + "." + _cache_key
            if redis_store.exists(cache_key):
                return redis_store.get(cache_key)
            token = func(*args, **kw)
            if token:
                redis_store.set(cache_key, token, expires)
            return token

        return deco

    return _cache_function


def cache_by_callback(callback, prefix="", expires=600):
    """ 通过callback获取 _cache_key"""

    def _cache_function(func):
        _prefix = prefix if prefix else func.__name__

        @wraps(func)
        def deco(*args, **kw):
            if not callable(callback):
                raise TypeError("callback must be function.")
            cache_key = _prefix + ":" + callback(*args, **kw)
            if redis_store.exists(cache_key):
                return redis_store.get(cache_key)
            else:
                ret = func(*args, **kw)
                redis_store.set(cache_key, ret, expires)
                return ret

        return deco

    return _cache_function


def token_required(func):
    """ token认证"""

    @wraps(func)
    def deco(*args, **kw):
        token = request.cookies.get("TOKEN")
        test_usercode = current_app.config.get("TEST_USERCODE")
        if test_usercode:
            return func(*args, **kw)

        if not token:
            return jsonify({"status": 401, "errmsg": "NotFoundToken", "data": {}})
        try:
            decrypt_token(token)
        except Exception:
            return jsonify({"status": 401, "errmsg": "InvalidToken", "data": {}})

        return func(*args, **kw)

    return deco


def role_required(*access):
    """权限认证"""

    def role_decorator(func):
        @wraps(func)
        def jug_authority(*args, **kwargs):
            current_user = EmployeeService.current_user()
            role_id = current_user.role_id
            role = Role.query.filter_by(id=role_id).first()
            role_name = role.name
            if access:
                if role_name not in access[0]:
                    return api_jsonify.no(
                        {"status": 401, "errmsg": "InvalidAuthority", "data": {}}
                    )
                elif role_name in access[0]:
                    if role_name == "AREAADMIN":
                        if func.__name__ == "get_areas":
                            Manager_info = AreaManager.query.filter_by(
                                usercode=current_user.usercode
                            ).first()
                            areas = Area.query.filter_by(id=Manager_info.area_id).all()
                            areas = [
                                {
                                    "id": area.id,
                                    "name": area.area_name,
                                    "state": area.disabled,
                                    "eveningends": area.eveningends,
                                    "morningstarts": area.morningstarts,
                                }
                                for area in areas
                            ]
                            for i in areas:
                                if i["state"] == 0:
                                    i["state"] = "开放"
                                else:
                                    i["state"] = "关闭"
                            return api_jsonify.ok(areaList=areas)
                        elif func.__name__ == "get_area":
                            Manager_info = AreaManager.query.filter_by(
                                usercode=current_user.usercode
                            ).first()
                            areas = Area.query.filter_by(id=Manager_info.area_id).all()
                            areas = [
                                {
                                    "id": area.id,
                                    "name": area.area_name,
                                    "state": area.disabled,
                                    "eveningends": area.eveningends,
                                    "morningstarts": area.morningstarts,
                                    "allow_time": str(area.morningstarts)
                                    + "点-"
                                    + str(area.eveningends)
                                    + "点",
                                    "time1": str(area.morningstarts) + "点",
                                    "time2": str(area.eveningends) + "点",
                                }
                                for area in areas
                            ]
                            for i in areas:
                                if i["state"] == 0:
                                    i["state"] = "开放"
                                else:
                                    i["state"] = "关闭"
                            return api_jsonify.ok(areaList=areas)
            return func(*args, **kwargs)

        return jug_authority

    return role_decorator
