# coding=utf-8
import calendar
import datetime
import hashlib
import random
import time

import jwt
from flask import current_app

LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"


def make_sha1_hash(text):
    sha1_hash = hashlib.sha1(text).hexdigest()
    return sha1_hash


def make_rnd_text(length=8):
    lst = [random.choice(LETTERS) for _ in range(length)]
    return "".join(lst)


def encrypt_token(usercode):
    """ 通过usercode获取token"""
    secret_key = current_app.config.get("SECRET_KEY")
    return jwt.encode(
        {"usercode": usercode, "timestamp": int(time.time())},
        secret_key,
        algorithm="HS256",
    )


def decrypt_token(token):
    secret_key = current_app.config.get("SECRET_KEY")
    return jwt.decode(token, secret_key, algorithms=["HS256"])


def datetime_str_to_timestamp(datetime_str, fmt="%Y-%m-%d %H:%M"):
    """ 日期字符串转换为时间戳"""
    dt = datetime.datetime.strptime(datetime_str, fmt)
    return int(time.mktime(dt.timetuple()))


def timestamp_to_datetime_str(timestamp, fmt="%Y-%m-%d %H:%M"):
    """ 时间戳转换为日期字符串"""
    dt = datetime.datetime.fromtimestamp(timestamp)
    return dt.strftime(fmt)


def get_date_range(start_date, end_date, fmt="%Y-%m-%d", step=1):
    """ 获取日期范围
    :param start_date: 开始日期
    :param end_date: 结束日期
    :param fmt: 格式
    :param step: 间隔
    """
    start_date = datetime.datetime.strptime(start_date, fmt)
    end_date = datetime.datetime.strptime(end_date, fmt)
    delta = datetime.timedelta(days=step)

    result = []

    while start_date <= end_date:
        result.append(start_date.strftime(fmt))
        start_date += delta

    return result


def get_date_week(date, fmt="%Y-%m-%d"):
    """ 获取某一日期是星期几
    周日为0
    """
    dt = datetime.datetime.strptime(date, fmt)
    w = calendar.weekday(dt.year, dt.month, dt.day)
    w = 0 if w == 6 else w + 1
    return w


def get_date_day(date, fmt="%Y-%m-%d"):
    """ 获取天数"""
    dt = datetime.datetime.strptime(date, fmt)
    return dt.day
