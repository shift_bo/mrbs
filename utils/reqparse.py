# coding=utf-8
from flask import request


class Parser(object):
    """ 请求解释器"""

    def __init__(self):
        self._fields = []

    def add(self, field, **kwargs):
        """ 添加参数"""
        self._fields.append(
            {
                "field": field,
                "required": kwargs.get("required", False),
                "default": kwargs.get("default"),
                "type": kwargs.get("type", str),
                "rename": kwargs.get("rename"),
                "location": kwargs.get("location", ["json", "form"]),
                "help": kwargs.get("help"),
            }
        )

    def get_field_value(self, field_name, location):
        """ 获取字段的值"""
        value = None
        if not isinstance(location, list):
            location = [location]

        for l in location:
            func = getattr(request, l, None)
            if callable(func):
                func = func()

            if not func:
                continue
            value = func.get(field_name)
            if value:
                break
        return value

    def args(self):
        """ 解析请求"""
        data = {}
        for field_item in self._fields:
            field_name = field_item["field"]
            field_value = self.get_field_value(field_name, field_item["location"])
            new_field_name = (
                field_item["rename"] if field_item["rename"] else field_name
            )
            field_type = field_item["type"]
            field_help = field_item["help"]
            if not field_value:
                if field_item["default"] is not None:
                    field_value = field_item["default"]
            else:
                if not isinstance(field_value, field_type):
                    try:
                        field_value = field_type(field_value)
                    except (ValueError, TypeError):
                        if isinstance(field_value, str):
                            field_value = field_value
                        elif isinstance(field_value, unicode):
                            field_value = str(field_value.encode("utf8"))

            if field_value is not None:
                data[new_field_name] = field_value
            elif not field_value and field_item["required"]:
                field_help = field_help or "NotFoundField: `{}`".format(field_name)
                return False, field_help
        return True, data
