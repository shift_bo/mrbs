# coding=utf-8
from api.base import RestApi
from api.base import RestView
from api.handler.room import RoomHandler
from service.room import AreaService
from service.room import RoomService
from utils.decorators import token_required


api = RestApi('app_room', __name__)


@api.route('/areas')
class AreaListView(RestView):
    """ 区域列表"""

    decorators = [token_required]

    def get(self):
        areas = AreaService.get_list()
        areas = [{"id": area.id, "name": area.area_name} for area in areas]
        return self.ok(areaList=areas)


@api.route('/areas/rooms')
class AreaRoomListView(RestView, RoomHandler):
    """ 区域会议室列表"""

    decorators = [token_required]

    def get(self):
        data = self.parse_room_list_data()
        rooms = RoomService.get_area_room_list(data["name"], data["date"])
        return self.ok(roomList=rooms)


@api.route('/areas/rooms/<int:room_id>')
class AreaRoomView(RestView, RoomHandler):
    """ 获取区域会议室"""
    decorators = [token_required]

    def get(self, room_id):
        data = self.parse_room_data()
        room = RoomService.get_area_room_by_room_id(room_id, data["date"])
        return self.ok(room=room)


@api.route('/devices')
class DeviceListView(RestView):
    """ 设备列表"""

    decorators = [token_required]

    def get(self):
        devices = RoomService.get_devices()
        return self.ok(devices=devices)
