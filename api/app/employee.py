# coding=utf-8
from flask import g

from api.base import RestApi
from api.base import RestView
from service.employee import DepartmentService
from service.employee import EmployeeService
from utils.decorators import token_required

api = RestApi('app_employee', __name__)


class Helper(object):
    @classmethod
    def department_to_json(cls, item):
        return {
            "id": item.id,
            "name": item.name,
            "parentId": item.parent_id,
            "type": "DEPARTMENT",
        }

    @classmethod
    def employee_to_json(cls, item):
        return {
            "id": item.id,
            "name": item.name,
            "usercode": item.usercode,
            "avatar": item.avatar,
            "departmentId": item.department_id,
            "type": "EMPLOYEE",
            "roleId": item.role_id,
            "roleName": "",
        }


@api.before_app_request
def _get_current_user():
    """ 全局设置"""
    if not hasattr(g, "user"):
        g.user = EmployeeService.current_user()


@api.route('/employees')
class EmployeeListView(RestView):
    """ 员工列表"""
    decorators = [token_required]

    def get(self):
        employees = EmployeeService.get_list()
        to_dict = Helper.employee_to_json
        return self.ok(employeeList=[to_dict(item) for item in employees])


@api.route('/employees/<name>')
class EmployeeView(RestView):
    """ 获取符合的员工列表"""
    decorators = [token_required]

    def get(self, name):
        employees = EmployeeService.get_list_by_name(name)
        to_dict = Helper.employee_to_json
        return self.ok(orgnazation=[to_dict(item) for item in employees])


@api.route('/currentuser')
class CurrentUserView(RestView):
    """ 当前登录员工"""

    decorators = [token_required]

    def get(self):
        current_user = EmployeeService.current_user()
        if not current_user:
            return self.no('NotFoundCurrentUser')
        to_dict = Helper.employee_to_json
        currentUser = to_dict(current_user)
        roleId = int(currentUser["roleId"])
        role = EmployeeService.get_by_roleid(roleId)
        currentUser["roleName"] = role.name
        return self.ok(currentUser=currentUser)


@api.route('/orgnazation')
@api.route('/orgnazation/<int:department_id>', endpoint='orgnazation')
class OrgnazationView(RestView):
    """ 组织结构"""

    decorators = [token_required]

    def get(self, department_id=0):
        departments = DepartmentService.get_list(parent_id=department_id, deleted=False)
        employees = EmployeeService.get_list(department_id=department_id, deleted=False)
        orgnazation = []
        for department in departments:
            orgnazation.append(Helper.department_to_json(department))

        for employee in employees:
            orgnazation.append(Helper.employee_to_json(employee))

        return self.ok(orgnazation=orgnazation)
