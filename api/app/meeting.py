# coding=utf-8
import json

from flask import g

from api.base import api_jsonify as jsonify
from api.base import RestApi
from api.base import RestView
from api.handler.meeting import MeetingHandler
from service.entry import EntryService
from service.meeting import MeetingService
from service.meeting import RepeatService
from tasks.notice import send_cancel_message
from tasks.notice import send_new_message
from tasks.notice import send_notice_by_meeting
from tasks.notice import send_notice_by_repeat
from utils import datetime_str_to_timestamp as d_to_t
from utils import timestamp_to_datetime_str as t_to_d
from utils.decorators import token_required
from logger import logger
api = RestApi('app_meeting', __name__)


@api.route('/meetings')
class MeetingListView(RestView, MeetingHandler):

    decorators = [token_required]

    def get(self):
        """ 获取与自己相关的会议列表"""
        data = self.get_args_data()
        meetings = MeetingService.get_list_by_current_user(
            g.user, selected=data["selected"], page=data["page"], size=data["size"]
        )
        meetings = [self.to_dict(item) for item in meetings]
        meetings = MeetingService.bind_entry_list_location(meetings)
        meetings = MeetingService.bind_entry_list_owner(meetings)
        return jsonify.ok(meetingList=meetings)

    def post(self):
        """ 创建会议"""
        data = self.get_form_data()
        rep_opt = self.get_rep_opt(data.rep_week)
        data["rep_opt"] = rep_opt
        data["month_absolute"] = data["rep_day"]
        data["create_by"] = g.user.username
        rep_type = data["rep_type"]

        """
        检测预定日期
        无重复 当天不得超过一个月
        天 不得超过一周
        周 不得超过一月
        月 不得超过半年
        
        """
        try:
            status,errmsg = RepeatService.check_meeting_date(data)
            if status:
                logger.error(errmsg)
                return self.ok(errmsg=errmsg,checkFailed=True)
        except:
            pass


        if RepeatService.is_repeat(rep_type):
            # 获取重复的日期
            date_range = self.get_rep_date_list(data)
            # 检测重复日期的冲突
            is_exists, meetings = RepeatService.check_exists(
                data.room_id, data.start_time, data.end_time, date_range
            )
            # 如果没有冲突， 直接执行
            if not is_exists:
                data["action"] = RepeatService.ACT_EXECUTE

            if not RepeatService.is_check(data.action):
                start_time = d_to_t(date_range[0] + " " + data["start_time"].split()[1])
                end_time = d_to_t(data["end_time"])
                end_date = d_to_t(data["end_date"] + " 23:59")
                rep = RepeatService.create(
                    data.room_id, data.name, data.description, start_time,
                    end_time, end_date, rep_type=rep_type, rep_opt=rep_opt,
                    month_absolute=data.month_absolute, create_by=data.create_by
                )

                meetings, exists_meetings = RepeatService.create_repeat_meeting(
                    rep, date_range, **data
                )
                meeting = MeetingService.get_entry_info(meetings[0])
                repeat_info = RepeatService.get_repeat_info(rep.id)
                repeat_info["skip_list"] = [item.start_time for item in exists_meetings]
                send_new_message.delay(meeting, repeat_info=repeat_info)
                return self.ok()
            else:
                return self.ok(
                    isExists=is_exists,
                    meetingList=[self.to_dict(item) for item in meetings],
                )

        else:
            # 普通会议
            status, meeting = MeetingService.new(**data)
            if status:
                meeting = MeetingService.get_entry_info(meeting)
                send_new_message.delay(meeting)
                return jsonify.ok()
            else:
                errmsg = "新的预定将与会议: {name} ({start_time} - {end_time}) 冲突。".format(
                    name=meeting.name,
                    start_time=t_to_d(meeting.start_time, fmt="%Y年%m月%d日 %H:%M"),
                    end_time=t_to_d(meeting.end_time, fmt="%Y年%m月%d日 %H:%M"),
                )
                return self.no(errmsg)


@api.route('/meetings/<int:mid>/info')
class MeetingInfoView(RestView):
    """ 获取会议预订信息"""

    decorators = [token_required]

    def get(self, mid):
        entry = EntryService.get_by_id(mid)
        owner = EntryService.get_owner(mid)
        info = self.to_dict(entry)
        if owner:
            info['owner'] = owner[1].name
        return self.ok(info=info)

    def to_dict(self, item):
        return {
            'name': item.name,
            'owner': item.create_by,
            'startTime': t_to_d(item.start_time, fmt='%H:%M'),
            'endTime': t_to_d(item.end_time, fmt='%H:%M'),
        }


@api.route('/meetings/<int:mid>')
class MeetingApiView(RestView, MeetingHandler):

    decorators = [token_required]

    def get(self, mid):
        """ 会议详情"""
        meeting = MeetingService.get_by_id(mid)
        meeting = MeetingService.bind_entry_location(self.to_dict(meeting))
        meeting = MeetingService.bind_entry_notice(meeting)
        meeting["employees"] = MeetingService.bind_employee_info_list_by_id(mid)
        meeting["orgnazation"] = MeetingService.bind_orgnazation_by_id(mid)
        return jsonify.ok(meeting=meeting)

    def post(self, mid):
        """ 编辑会议"""
        data = self.get_form_data()
        rep_opt = self.get_rep_opt(data.rep_week)
        data["rep_opt"] = rep_opt
        data["month_absolute"] = data["rep_day"]
        data["create_by"] = g.user.username

        rep_type = data.rep_type

        try:
            status,errmsg = RepeatService.check_meeting_date(data)
            if status:
                logger.error(errmsg)
                return self.ok(errmsg=errmsg,checkFailed=True)
        except:
            pass

        # 编辑重复会议
        if data.repeat_id:
            date_range = self.get_rep_date_list(data)
            # 检测是否冲突
            is_exists, meetings = RepeatService.check_exists(
                data.room_id, data.start_time, data.end_time,
                date_range, repeat_id=data.repeat_id,
            )
            # 如果没有冲突， 执行程序
            if not is_exists:
                data["action"] = RepeatService.ACT_EXECUTE

            if RepeatService.is_repeat(rep_type):
                if not RepeatService.is_check(data["action"]):
                    # 删除原有的重复会议
                    start_time = d_to_t(
                        date_range[0] + " " + data["start_time"].split()[1]
                    )
                    end_time = d_to_t(data["end_time"])
                    end_date = d_to_t(data["end_date"] + " 23:59")
                    RepeatService.delete_repeat_meeting(data["repeat_id"])
                    rep = RepeatService.update_by_id(
                        data.repeat_id,
                        data.room_id,
                        data.name,
                        data.description,
                        start_time,
                        end_time,
                        end_date,
                        rep_type=rep_type,
                        rep_opt=rep_opt,
                        modified_by=data.create_by,
                        month_absolute=data.month_absolute,
                    )

                    meetings, exists_meetings = RepeatService.create_repeat_meeting(
                        rep, date_range, **data
                    )
                    if len(meetings) > 0:
                        meeting = MeetingService.get_entry_info(meetings[0])
                        skip_list = [item.start_time for item in exists_meetings]
                        org = json.loads(data["org"])
                        send_notice_by_repeat(
                            rep.id, org, g.user.usercode, skip_list=skip_list
                        )
                    return jsonify.ok()

            else:
                RepeatService.delete_by_id(data.repeat_id)
                RepeatService.delete_repeat_meeting(data.repeat_id)
                data.pop("repeat_id")
                status, meeting = MeetingService.new(**data)
                if status:
                    meeting = MeetingService.get_entry_info(meeting)
                    send_new_message.delay(meeting)
                    return jsonify.ok()
                else:
                    errmsg = "新的预定将与会议: {name} ({start_time} - {end_time}) 冲突。".format(
                        name=meeting.name,
                        start_time=t_to_d(meeting.start_time, fmt="%Y年%m月%d日 %H:%M"),
                        end_time=t_to_d(meeting.end_time, fmt="%Y年%m月%d日 %H:%M"),
                    )
                    return jsonify.no(errmsg)

            return jsonify.ok(
                isExists=is_exists,
                meetingList=[self.to_dict(item) for item in meetings],
            )

        else:
            data.pop("repeat_id")
            MeetingService.edit(mid, **data)
            org = json.loads(data["org"])
            send_notice_by_meeting.delay(mid, org, g.user.usercode)
        return jsonify.ok()

    def options(self, mid):
        return jsonify.ok()

    def delete(self, mid):
        """ 取消会议"""
        meeting_info = MeetingService.get_entry_info(MeetingService.get_by_id(mid))
        delete_usercodes = MeetingService.cancel(mid)
        send_cancel_message.delay(meeting_info, delete_usercodes)
        return jsonify.ok()


@api.route('/meetings/<int:mid>/employee/accept')
class EmployeeAcceptView(RestView):
    """ 接受会议"""

    decorators = [token_required]

    def post(self, mid):
        MeetingService.set_entry_employee_status_accept(mid, g.user.usercode)
        return jsonify.ok()


@api.route('/meetings/<int:mid>/employee/rest')
class EmployeeRestView(RestView):
    """ 请假"""

    decorators = [token_required]

    def post(self, mid):
        MeetingService.set_entry_employee_status_rest(mid, g.user.usercode)
        return jsonify.ok()


@api.route('/repeat/<int:repeat_id>/info')
class RepeatMeetingView(RestView):
    """ 获取序列会议信息"""

    decorators = [token_required]

    def get(self, repeat_id):
        meeting = RepeatService.get_by_id(repeat_id)
        if not meeting:
            return jsonify.ok()
        result = dict(
            startDate=t_to_d(meeting.start_time, fmt="%Y-%m-%d"),
            repType=meeting.rep_type,
            endDate=t_to_d(meeting.end_date, fmt="%Y-%m-%d"),
            repDay=meeting.month_absolute or "",
            repWeek=meeting.rep_opt.find("1"),
        )
        return jsonify.ok(repeatInfo=result)
