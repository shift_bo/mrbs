# coding=utf-8
from flask import current_app
from flask import make_response
from flask import redirect
from flask import request

from api.base import RestApi
from api.base import RestView
from service.ext.weixin import WXService
from utils import encrypt_token

api = RestApi('api_weixin', __name__)


@api.route('/weixin/config')
class WXConfigView(RestView):
    """ 获取微信配置"""

    def post(self):
        path = request.json.get('path') or '/'
        jssdk_config = WXService.get_jssdk_config(path)
        return self.ok(**jssdk_config)


@api.route('/weixin/oauth2/web')
class WXOauthWebLinkView(RestView):
    """ 微信网页授权链接"""

    def get(self):
        redirect_uri = current_app.config.get("WX_OAUTH2_CALLBACK")
        return redirect(WXService.make_web_oauth2_link(redirect_uri=redirect_uri))


@api.route('/weixin/oauth2/callback')
class WXOauthCallbackView(RestView):
    """ 微信网页授权回调地址"""

    def get(self):
        code = request.args.get("code")
        usercode = WXService.get_visitor_info(code)
        response = make_response()
        if not usercode:
            response.set_cookie("TOKEN", "", max_age=0)
        else:
            token = encrypt_token(usercode)
            response.set_cookie("TOKEN", token, max_age=86400 * 7)
        response.headers["Location"] = "/"
        return response, 302
