# coding=utf-8
from api.base import Parser


class RoomHandler(object):

    @classmethod
    def parse_room_list_data(cls):
        parser = Parser()
        parser.add("name", required=True, location="args")
        parser.add("date", required=True, location="args")
        return parser.get_values()

    @classmethod
    def parse_room_data(cls):
        parser = Parser()
        parser.add("date", required=True, location="args")
        return parser.get_values()
