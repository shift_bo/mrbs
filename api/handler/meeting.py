# coding=utf-8
import datetime

from api.base import Parser
from service.meeting import RepeatService
from utils import get_date_day
from utils import get_date_range
from utils import get_date_week


class MeetingHandler(object):

    @classmethod
    def get_form_data(cls):
        parser = Parser()
        parser.add("repeatId", type=int, rename="repeat_id", ignore=True)
        parser.add("name", required=True)
        parser.add("description", default="")
        parser.add("startTime", required=True, rename="start_time")
        parser.add("endTime", required=True, rename="end_time")
        parser.add("roomId", required=True, type=int, rename="room_id")
        parser.add("org", required=True)
        parser.add("noticeType", required=True, rename="notice_type")
        parser.add("repType", required=True, type=int, default=0, rename="rep_type")
        parser.add("repWeek", type=int, default=-1, rename="rep_week", ignore=True)
        parser.add("repDay", type=int, default=0, rename="rep_day", ignore=True)
        parser.add(
            "endDate",
            default=datetime.datetime.now().strftime("%Y-%m-%d"),
            rename="end_date",
        )
        parser.add("action", default="check")
        return parser.get_values()

    @classmethod
    def get_args_data(cls):
        parser = Parser()
        parser.add("selected", default="", location="args")
        parser.add("page", type=int, default=1, location="args")
        parser.add("size", type=int, default=20, location="args")
        return parser.get_values()

    @classmethod
    def to_dict(cls, item):
        return {
            "id": item.id,
            "name": item.name,
            "roomId": item.room_id,
            "description": item.description,
            "startTime": item.start_time * 1000,
            "endTime": item.end_time * 1000,
            "createBy": item.create_by,
            "repeatId": item.repeat_id or "",
        }

    @classmethod
    def get_rep_opt(cls, rep_week):
        # 兼容meeting老系统
        if rep_week > -1:
            rep_opt = ['0'] * 7
            rep_opt[rep_week] = '1'
            rep_opt = ''.join(rep_opt)
        else:
            rep_opt = '0'
        return rep_opt

    @classmethod
    def get_date_list(cls, start_date, end_date):
        """ 获取重复的日期列表
        :param start_date: 开始日期
        :param end_date: 结束日期
        """
        return get_date_range(start_date, end_date)

    @classmethod
    def get_date_range_by_day(cls, start_time, end_date):
        start_date = start_time.split()[0]
        return get_date_range(start_date, end_date)

    @classmethod
    def get_date_range_by_week(cls, start_time, end_date, rep_week):
        """ 重复的星期
        :param rep_week: 重复的星期
        """
        start_date = start_time.split()[0]
        date_range = get_date_range(start_date, end_date)
        date_range = [date for date in date_range if get_date_week(date) == rep_week]
        return date_range

    @classmethod
    def get_date_range_by_month(cls, start_time, end_date, rep_day):
        """ 重复的日期
        :param repday: 重复的日期
        """
        start_date = start_time.split()[0]
        date_range = get_date_range(start_date, end_date)
        date_range = [date for date in date_range if get_date_day(date) == rep_day]
        return date_range

    @classmethod
    def get_rep_date_list(cls, data):
        """ 获取重复日期列表"""
        rep_type = data.rep_type
        start_time = data.start_time
        end_date = data.end_date

        if RepeatService.is_repeat_day(data.rep_type):
            date_range = cls.get_date_range_by_day(start_time, end_date)
        elif RepeatService.is_repeat_week(rep_type):
            date_range = cls.get_date_range_by_week(
                start_time, end_date, data.rep_week
            )
        elif RepeatService.is_repeat_month(rep_type):
            date_range = cls.get_date_range_by_month(
                start_time, end_date, data.rep_day
            )
        else:
            date_range = [start_time.split()[0]]
        return date_range
