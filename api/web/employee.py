# coding=utf8
# flake8: noqa
# noqa: all
import sys

from flask import Blueprint
from flask import request

from api.base import api_jsonify as jsonify
from models import Area
from models import AreaManager
from models import db
from models import Employee
from models import Role
from service.employee import EmployeeService
from tasks.employee import sync
from utils.decorators import role_required
from utils.decorators import token_required
reload(sys)
sys.setdefaultencoding('utf-8')

bp = Blueprint('web_employee', __name__)


@bp.route('/employees/userInfo')
@role_required(['SUPERADMIN'])
def get_all_userinfo():
    userlist = Employee.query.filter_by().all()
    user_info = [{'username': item.username, 'name': item.name, 'email': item.email, 'usercode': item.usercode, 'role_id': item.role_id, 'id': item.id,
                  'role_display_name': query_role_name(item.role_id, item.usercode)[0], 'role_name':query_role_name(item.role_id, item.usercode)[1]}for item in userlist]
    return jsonify.ok(userInfo=user_info)


@bp.route('/employees/adduserInfo', methods=['GET', 'POST'])
@role_required(['SUPERADMIN'])
def add_userinfo():
    if request.method == 'POST':
        new_data = Employee(
            usercode=request.json.get('usercode'),
            username=request.json.get('username'),
            name=request.json.get('name'),
            email=request.json.get('email'),
            role_id=2
        )
        db.session.add(new_data)
        db.session.commit()
        return jsonify.ok()
    else:
        return jsonify.no("更新失败")


@bp.route('/employees/changeuserInfo', methods=['GET', 'POST'])
@role_required(['SUPERADMIN'])
def change_user_info():
    if request.method == "POST":
        usercode = request.json.get('usercode')
        username = request.json.get('username')
        name = request.json.get('name')
        email = request.json.get('email')
        role_info = request.json.get('role')
        role_ob = Role.query.filter_by(name=role_info).first()
        area_id = request.json.get('area_id')
        if role_info == 'AREAADMIN':
            index = AreaManager.query.filter_by(usercode=usercode).first()
            if index:
                AreaManager.query.filter_by(usercode=usercode).update({'usercode': usercode, 'area_id': area_id})
                db.session.commit()
            else:
                new_manager = AreaManager(area_id=area_id, usercode=usercode)
                db.session.add(new_manager)
                db.session.commit()
        id = request.json.get('id')
        Employee.query.filter_by(id=id).update({'usercode': usercode, 'username': username, 'name': name, 'email': email, 'role_id': role_ob.id})
        db.session.commit()
        return jsonify.ok()


@bp.route('/employees/deluserInfo', methods=['GET', 'POST'])
@role_required(['SUPERADMIN'])
def del_user_info():
    id = request.json.get('id')
    usercode = request.json.get('usercode')
    role_info = request.json.get('role')
    if role_info == 'AREAADMIN':
        del_data = AreaManager.query.filter_by(usercode=usercode).one()
        db.session.delete(del_data)
        db.session.commit()
    del_data = Employee.query.filter_by(id=id).one()
    db.session.delete(del_data)
    db.session.commit()
    return jsonify.ok()


@bp.route('/employees/Role', methods=['GET', 'POST'])
@role_required(['SUPERADMIN'])
def get_role():
    if request.method == 'GET':
        role_info = Role.query.filter_by().all()
        role_info = [{'id': item.id, 'name': item.name, 'display_name': item.display_name} for item in role_info]
        return jsonify.ok(role_info=role_info)
    elif request.method == 'POST':
        role_id = request.json.get('role_id')
        Role_info = Role.query.filter_by(id=role_id).first()
        return jsonify.ok(role_name=Role_info.name)


class Helper(object):
    @classmethod
    def department_to_json(cls, item):
        return {
            "id": item.id,
            "name": item.name,
            "parentId": item.parent_id,
            "type": "DEPARTMENT",
        }

    @classmethod
    def employee_to_json(cls, item):
        return {
            "id": item.id,
            "name": item.name,
            "usercode": item.usercode,
            "avatar": item.avatar,
            "departmentId": item.department_id,
            "type": "EMPLOYEE",
            "role_id": item.role_id
        }


@bp.route("/currentuser")
@token_required
def get_current_user():
    """ 当前员工"""
    current_user = EmployeeService.current_user()
    to_dict = Helper.employee_to_json
    data = to_dict(current_user)
    role_name = Role.query.filter_by(id=data['role_id']).first()
    data['role_name'] = role_name.name
    return jsonify.ok(currentUser=data)


def query_role_name(role_id, usercode):
    role_info = Role.query.filter_by(id=role_id).first()
    if role_info.name == 'AREAADMIN':
        area_info = AreaManager.query.filter_by(usercode=usercode).first()
        area_info = Area.query.filter_by(id=area_info.area_id).first()
        return [role_info.display_name + '(' + area_info.area_name + ')', role_info.name]
    if not role_info:

        return ['', '']
    return [role_info.display_name, role_info.name]


@bp.route('/userlist/update')
@token_required
@role_required(['SUPERADMIN'])
def sync_update():
    sync.delay()
    return jsonify.ok()
