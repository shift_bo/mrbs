# coding=utf8
# flake8: noqa
# noqa: all
import sys

from flask import Blueprint
from flask import request

from api.base import api_jsonify as jsonify
from models import Area
from models import db
from models import Device
from models import Room
from models import RoomDevice
from service.room import AreaService
from service.room import RoomService
from utils.decorators import role_required
from utils.decorators import token_required
from utils.reqparse import Parser
reload(sys)
sys.setdefaultencoding('utf-8')

bp = Blueprint('web_room', __name__)


class Helper(object):

    @classmethod
    def get_form_data(cls):
        parser = Parser()
        parser.add('name', required=True, location='args')
        parser.add('date', required=True, location='args')
        return parser.args()

    @classmethod
    def get_args_data(cls):
        parser = Parser()
        parser.add('date', required=True, location='args')
        return parser.args()


@bp.route('/areas/getareas')
@role_required(['ADMIN', 'AREAADMIN', 'SUPERADMIN'])
def get_area():
    """ 区域列表"""
    areas = Area.query.filter_by().all()
    areas = [{'id': area.id, 'name': area.area_name, 'state': area.disabled, 'eveningends': area.eveningends, 'morningstarts': area.morningstarts, 'allow_time':
              str(area.morningstarts) + '点-' + str(area.eveningends) + '点', 'time1': str(area.morningstarts) + '点', 'time2': str(area.eveningends) + '点'} for area in areas]
    for i in areas:
        if i['state'] == 0:
            i['state'] = '开放'
        else:
            i['state'] = '关闭'

    return jsonify.ok(areaList=areas)


@bp.route('/areas')
@role_required(['ADMIN', 'AREAADMIN', 'SUPERADMIN'])
def get_areas():
    """ 区域列表"""
    areas = AreaService.get_list()
    areas = [{'id': area.id, 'name': area.area_name, 'state': area.disabled, 'eveningends': area.eveningends, 'morningstarts': area.morningstarts} for area in areas]
    for i in areas:
        if i['state'] == 0:
            i['state'] = '开放'
        else:
            i['state'] = '关闭'

    return jsonify.ok(areaList=areas)


@bp.route('/areas/rooms/s', methods=['GET', 'POST'])
@role_required(['ADMIN', 'AREAADMIN', 'SUPERADMIN'])
def get_areas_rooms():
    if request.method == 'GET':
        area_id = request.args.get('area_id')
        if request.args.get('area_id'):
            if request.args.get('area_id') == 'all':
                rooms = Room.query.all()
                rooms = [format_room_data(item) for item in rooms]
            else:
                rooms = Room.query.filter_by(area_id=area_id)
                rooms = [format_room_data(item) for item in rooms]

            Rooms = []
            for i in rooms:
                device_cache = []
                device_data = RoomDevice.query.filter_by(room_id=i['room_id']).all()
                for y in device_data:
                    device = Device.query.filter_by(id=y.device_id).first()
                    try:
                        device_cache.append(device.name)
                    except:
                        pass
                i['device'] = ','.join(device_cache)
                Rooms.append(i)
            return jsonify.ok(Rooms=Rooms)
    elif request.method == 'POST':
        data = Room.query.filter_by().all()
        id_list = [i.id for i in data]
        id = 1
        while True:
            if id in id_list:
                id += 1
            else:
                if Room.query.filter_by(room_name=room_name).first() != None:
                    return jsonify.no('会议室名称重复')
                ob = Area.query.filter_by(area_name=area_name).one()
                room_info = Room(room_name=room_name, capacity=capacity, area_id=ob.id, id=id)
                for device_id in interest:
                    room_device_info = RoomDevice(room_id=id, device_id=device_id)
                    db.session.add(room_device_info)
                db.session.add(room_info)
                db.session.commit()
                return jsonify.ok()


@bp.route('/areas/rooms/change', methods=['GET', 'POST'])
@role_required(['ADMIN', 'AREAADMIN', 'SUPERADMIN'])
def change_room_disabled():
    if request.method == 'post':
        key_word = Room.query.filter_by(room_name=request.json.get('room_name')).one()
        key_word = key_word.disabled
        if key_word == '开放':
            Room.query.filter_by(room_name=request.json.get('room_name')).update({"disabled": "关闭"})
            return jsonify.ok()
        else:
            Room.query.filter_by(room_name=request.json.get('room_name')).update({"disabled": "开放"})
            return jsonify.ok()


@bp.route('/areas/rooms/del', methods=['GET', 'POST'])
@role_required(['ADMIN', 'AREAADMIN', 'SUPERADMIN'])
def delet_room():
    room_name = request.json.get('room_name')
    room_id = request.json.get('room_id')
    rep = Room.query.filter_by(room_name=room_name).first()
    del_room_device = RoomDevice.query.filter_by(room_id=room_id).all()
    for i in del_room_device:
        db.session.delete(i)
    db.session.delete(rep)
    db.session.commit()
    return jsonify.ok()


@bp.route('/areas/rooms/changeInfo', methods=['GET', 'POST'])
@role_required(['ADMIN', 'AREAADMIN', 'SUPERADMIN'])
def chang_room_info():
    room_name = request.json.get('room_name')
    capacity = request.json.get('capacity')
    interest = request.json.get('interest')
    room_id = request.json.get('room_id')
    area_name = request.json.get('area_name')
    disabled = format_disabled(request.json.get('disabled'))
    Room.query.filter_by(id=room_id).update({'capacity': capacity, 'room_name': room_name, 'disabled': disabled})
    rd_result = RoomDevice.query.filter_by(room_id=room_id).all()
    device_id_list = [i.device_id for i in rd_result]
    try:
        if interest[0] == '':
            del interest[0]
    except:
        pass
    for device in device_id_list:
        if device not in interest:
            # device_id_list.remove(device)
            del_data = RoomDevice.query.filter_by(room_id=room_id, device_id=device).first()
            db.session.delete(del_data)
            db.session.commit()
    for device in interest:
        if device not in device_id_list:
            add_data = RoomDevice(room_id=room_id, device_id=device)
            db.session.add(add_data)
            db.session.commit()

    db.session.commit()
    for device in interest:
        if device not in device_id_list:
            add_data = RoomDevice(room_id=room_id, device_id=device)
            db.session.add(add_data)
            db.session.commit()
    db.session.commit()
    return jsonify.ok()


@bp.route('/areas/rooms/changeState', methods=['GET', 'POST'])
@role_required(['ADMIN', 'AREAADMIN', 'SUPERADMIN'])
def change_room_state():
    room_name = request.json.get('room_name')
    room = Room.query.filter_by(room_name=room_name).first()
    data = room.time_state.split('-')
    if data[-1] == '关闭':
        data[-1] = '打开'
    else:
        data[-1] = '关闭'
    time_state = '-'.join(data)
    Room.query.filter_by(room_name=room_name).update({'time_state': time_state})
    db.session.commit()
    return jsonify.ok()


@bp.route('/areas/rooms/changeAreaInfo', methods=['GET', 'POST'])
@role_required(['ADMIN', 'AREAADMIN', 'SUPERADMIN'])
def change_area_info():
    area_name = request.json.get('name')
    area_state = request.json.get('state')
    area_moningstarts = request.json.get('time1')[0:2]
    area_eveningends = request.json.get('time2')[0:2]
    if area_state == '关闭':

        area_state = 1
    else:
        area_state = 0
    index = request.json.get('index')
    areaList = Area.query.filter_by().all()
    Area.query.filter_by(area_name=areaList[index].area_name).update({'area_name': area_name, 'disabled': area_state, 'eveningends': area_eveningends,
                                                                      'morningstarts': area_moningstarts})
    db.session.commit()
    return jsonify.ok()


@bp.route('/areas/rooms/addAreaInfo', methods=['GET', 'POST'])
@role_required(['ADMIN', 'SUPERADMIN'])
def add_area_info():
    area_name = request.json.get('name')
    area_moningstarts = int(request.json.get('time1')[0:2])
    area_eveningends = int(request.json.get('time2')[0:2])
    area_state = request.json.get('state')
    if area_moningstarts > area_eveningends:
        return jsonify.no('时间不符合真是情况')
    if area_state == '关闭':
        area_state = 1
    else:
        area_state = 0
    area_info = Area(area_name=area_name, disabled=area_state, morningstarts=area_moningstarts, eveningends=area_eveningends)
    db.session.add(area_info)
    db.session.commit()
    return jsonify.ok()


@bp.route('/areas/rooms/device')
@role_required(['ADMIN', 'AREAADMIN', 'SUPERADMIN'])
def get_device():
    data = Device.query.filter_by().all()
    DeviceList = [{'id': item.id, 'name': item.name} for item in data]
    return jsonify.ok(device=DeviceList)


@bp.route('/areas/rooms/adddevice', methods=['GET', 'POST'])
@role_required(['ADMIN', 'SUPERADMIN'])
def add_device():
    deviceList = Device.query.filter_by().all()
    index = len(deviceList)
    device_name = request.json.get('name')
    data = Device(name=device_name)
    try:
        db.session.add(data)
        db.session.commit()
        return jsonify.ok()
    except:
        return jsonify.no('设备重复')


@bp.route('/areas/rooms/deldevice', methods=['GET', 'POST'])
@role_required(['ADMIN', 'SUPERADMIN'])
def delete_device():
    id = request.json.get('id')
    data = Device.query.filter_by(id=id).first()
    db.session.delete(data)
    db.session.commit()
    return jsonify.ok()


@bp.route('/areas/rooms/changedevicename', methods=['GET', 'POST'])
@role_required(['ADMIN', 'SUPERADMIN'])
def change_device_name():
    id = request.json.get('id')
    new_name = request.json.get('name')
    Device.query.filter_by(id=id).update({'name': new_name})
    db.session.commit()
    return jsonify.ok()


def format_room_data(obj):
    return {
        'id': obj.area_id,
        'room_name': obj.room_name,
        'capacity': obj.capacity,
        'description': obj.description,
        'room_id': obj.id,
        'disabled': format_disabled(obj.disabled)
    }


def format_disabled(info):
    '''
    格式化状态
    ::disabled转化值
    '''
    info = str(info)
    if info == '开放' or info == "关闭":
        if info == '开放':

            return '0'
        else:
            return '1'
    else:
        if info == '1':
            return '关闭'
        else:
            return '开放'
