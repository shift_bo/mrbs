# coding=utf-8
import copy
import json
import time
import uuid
import datetime
from dateutil.relativedelta import relativedelta

from flask import g
from sqlalchemy import and_
from sqlalchemy import or_

from models import Area
from models import db
from models import Department
from models import Employee
from models import Entry
from models import EntryDepartment
from models import EntryEmployee
from models import EntryNotice
from models import Repeat
from models import Room
from service.employee import EmployeeService
from utils import datetime_str_to_timestamp as d_to_t
from utils import get_date_day
from utils import get_date_range
from utils import get_date_week
from logger import logger

class MeetingService(object):
    @classmethod
    def get_list(cls):
        return Entry.query.all()

    @classmethod
    def get_by_id(cls, id):
        return Entry.query.filter_by(id=id).first()

    @classmethod
    def make_ical_uid(cls):
        return str(uuid.uuid1())

    @classmethod
    def get_by_own(cls,username):
        return Entry.query.filter_by(create_by = username).all()

    @classmethod
    def get_entry_info(cls, entry):
        """ 获取会议信息"""
        info = {
            "id": entry.id,
            "name": entry.name,
            "startTime": entry.start_time * 1000,
            "endTime": entry.end_time * 1000,
            "roomId": entry.room_id,
            "uid": entry.ical_uid,
        }
        info = cls.bind_entry_location(info)
        info = cls.bind_entry_notice(info)
        employees = cls.bind_employee_info_list_by_id(entry.id)
        info["employees"] = employees
        owner = [employee for employee in employees if employee["isOwner"]]
        info["owner"] = owner[0] if owner else ''

        return info

    @classmethod
    def new(cls, **kwargs):
        """ 新增会议"""
        start_time, end_time = kwargs["start_time"], kwargs["end_time"]
        start_time, end_time = d_to_t(start_time), d_to_t(end_time)
        exists_entry = cls.is_exists(kwargs["room_id"], start_time, end_time)

        if exists_entry:
            return False, exists_entry

        # 创建会议
        entry = Entry(
            name=kwargs["name"],
            description=kwargs["description"],
            room_id=kwargs["room_id"],
            start_time=start_time,
            end_time=end_time,
            ical_uid=kwargs.get("ical_uid") or cls.make_ical_uid(),
            ical_sequence=kwargs.get("ical_sequence", 0),
            create_by=kwargs["create_by"],
        )
        if kwargs.get("repeat_id"):
            entry.repeat_id = kwargs["repeat_id"]
        db.session.add(entry)
        db.session.commit()

        # 创建会议通知类型
        entry_notice = EntryNotice(entry_id=entry.id, type=kwargs["notice_type"])
        db.session.add(entry_notice)

        # 创建会议参会人员
        EmployeeService.create_entry_employees(
            entry.id, json.loads(kwargs["org"]), g.user.usercode
        )
        return True, entry

    @classmethod
    def edit(cls, id, **data):
        """ 编辑会议"""
        data["start_time"] = d_to_t(data["start_time"])
        data["end_time"] = d_to_t(data["end_time"])
        cls.update_by_id(id, **data)

        # 修改会议通知方式
        entry_notice = EntryNotice.query.filter_by(entry_id=id).first()
        entry_notice.type = data["notice_type"]
        db.session.commit()

    @classmethod
    def cancel(cls, id):
        """ 取消会议"""
        entry = cls.get_by_id(id)
        db.session.delete(entry)

        employees = EntryEmployee.query.filter_by(entry_id=id)
        delete_usercodes = [employee.usercode for employee in employees]

        for employee in employees:
            db.session.delete(employee)

        departments = EntryDepartment.query.filter_by(entry_id=id)

        for department in departments:
            db.session.delete(department)
        db.session.commit()
        return delete_usercodes

    @classmethod
    def bind_employee_info_list_by_id(cls, id):
        """ 绑定员工信息"""
        querys = (
            db.session.query(
                Employee.name,
                Employee.usercode,
                Employee.username,
                EntryEmployee.is_owner,
                EntryEmployee.status,
            )
                .join(EntryEmployee, Employee.usercode == EntryEmployee.usercode)
                .filter(EntryEmployee.entry_id == id)
        )

        return [
            {
                "name": item[0],
                "usercode": item[1],
                "username": item[2],
                "isOwner": item[3],
                "status": item[4],
            }
            for item in querys
        ]

    @classmethod
    def bind_orgnazation_by_id(cls, id):
        """ 绑定组织结构"""

        result = []
        department_query = (
            db.session.query(Department.id, Department.name, Department.parent_id)
                .join(EntryDepartment, Department.id == EntryDepartment.department_id)
                .filter(EntryDepartment.entry_id == id, EntryDepartment.deleted == 0)
        )

        for info in department_query:
            result.append(
                {
                    "id": info[0],
                    "name": info[1],
                    "parentId": info[2],
                    "type": "DEPARTMENT",
                }
            )

        employee_query = (
            db.session.query(
                Employee.id,
                Employee.name,
                Employee.usercode,
                Employee.avatar,
                Employee.department_id,
            )
                .join(EntryEmployee, Employee.usercode == EntryEmployee.usercode)
                .filter(
                EntryEmployee.entry_id == id,
                EntryEmployee.deleted == 0,
                EntryEmployee.from_department == 0,
            )
        )

        for info in employee_query:
            result.append(
                {
                    "id": info[0],
                    "name": info[1],
                    "usercode": info[2],
                    "avatar": info[3],
                    "departmentId": info[4],
                    "type": "EMPLOYEE",
                }
            )

        return result

    @classmethod
    def get_list_by_current_user(cls, user, selected="", page=1, size=20):
        """ 获取当前用户先关的会议列表"""
        entry_employee_list = EntryEmployee.query.filter_by(
            usercode=user.usercode, deleted=False
        )
        entry_ids = [item.entry_id for item in entry_employee_list]
        entries = Entry.query.filter_by(create_by=user.username)
        entry_ids.extend([item.id for item in entries])
        entry_ids = list(set(entry_ids))

        now = int(time.time())
        if selected.upper() == "CURRENT":
            result = (
                Entry.query.filter(
                    Entry.id.in_(entry_ids), Entry.end_time > now
                )
                    .order_by(Entry.timestamp.desc())
                    .offset((page - 1) * size)
                    .limit(size)
            )
        elif selected.upper() == "HISTORY":
            result = (
                Entry.query.filter(
                    Entry.id.in_(entry_ids), Entry.end_time <= now
                )
                    .order_by(Entry.timestamp.desc())
                    .offset((page - 1) * size)
                    .limit(size)
            )
        else:
            result = (
                Entry.query.filter(Entry.id.in_(entry_ids))
                    .order_by(Entry.timestamp.desc())
                    .offset((page - 1) * size)
                    .limit(size)
            )

        return result

    @classmethod
    def bind_entry_list_location(cls, entry_list):
        """ 绑定会议地点"""
        room_id_list = [entry["roomId"] for entry in entry_list]
        query = (
            db.session.query(Area.area_name, Room.room_name, Room.id)
                .join(Room, Area.id == Room.area_id)
                .filter(Room.id.in_(room_id_list))
        )

        infos = {
            info[2]: {
                "area": info[0],
                "room": info[1],
                "location": info[0] + " " + info[1],
            }
            for info in query
        }

        for entry in entry_list:
            entry.update(infos[entry["roomId"]])
        return entry_list

    @classmethod
    def bind_entry_list_owner(cls, entry_list):
        """ 绑定会议发起人"""
        entry_ids = [entry["id"] for entry in entry_list]
        usernames = [entry["createBy"] for entry in entry_list]
        usernames = list(set(usernames))
        querys = (
            db.session.query(Employee.name, EntryEmployee.entry_id)
                .join(EntryEmployee, EntryEmployee.usercode == Employee.usercode)
                .filter(EntryEmployee.entry_id.in_(entry_ids), EntryEmployee.is_owner == 1)
        )

        employees = Employee.query.filter(Employee.username.in_(usernames))

        entry_id_map = {info[1]: info[0] for info in querys}
        username_map = {item.username: item.name for item in employees}

        for entry in entry_list:
            entry["owner"] = entry_id_map.get(entry["id"]) or username_map.get(
                entry["createBy"]
            )

        return entry_list

    @classmethod
    def bind_entry_location(cls, entry):
        info = (
            db.session.query(Area.area_name, Room.room_name, Room.id)
                .join(Room, Area.id == Room.area_id)
                .filter(Room.id == entry["roomId"])
        )
        if info.count() > 0:
            info = info.first()
            entry.update(
                {"area": info[0], "room": info[1], "location": info[0] + " " + info[1]}
            )

        return entry

    @classmethod
    def bind_entry_notice(cls, entry):
        """ 绑定会议通知"""
        entry_notice = EntryNotice.query.filter_by(entry_id=entry["id"]).first()
        if entry_notice:
            entry["noticeType"] = entry_notice.type
        return entry

    @classmethod
    def update_by_id(cls, id, **kw):
        entry = cls.get_by_id(id)
        for k, v in kw.items():
            if hasattr(entry, k) and getattr(entry, k) != v:
                setattr(entry, k, v)

        db.session.commit()
        return entry

    @classmethod
    def update_entry_employee_status(cls, id, usercode, status):
        """ 更新会议员工状态"""
        entry_employee = EntryEmployee.query.filter_by(
            entry_id=id, usercode=usercode
        ).first()
        if entry_employee:
            entry_employee.status = status
            db.session.commit()

    @classmethod
    def set_entry_employee_status_accept(cls, id, usercode):
        cls.update_entry_employee_status(id, usercode, EntryEmployee.ST_ACCEPT)

    @classmethod
    def set_entry_employee_status_rest(cls, id, usercode):
        cls.update_entry_employee_status(id, usercode, EntryEmployee.ST_REST)

    @classmethod
    def is_exists(cls, room_id, st, et, exclude_repeat_id=None):
        """ 是否在当前时间内存在会议
        :param room_id: 会议室
        :param st: 开始时间
        :param et: 结束时间
        :param exclude_repeat_id: 排除的repeat_id
        """
        left = [
            Entry.room_id == room_id,
            Entry.start_time < st,
            Entry.end_time > st,
        ]
        contain = [
            Entry.room_id == room_id,
            Entry.start_time < et,
            Entry.end_time > et,
        ]
        right = [
            Entry.room_id == room_id,
            Entry.start_time >= st,
            Entry.end_time <= et,
        ]
        if exclude_repeat_id is not None:
            left.append(Entry.repeat_id != exclude_repeat_id)
            contain.append(Entry.repeat_id != exclude_repeat_id)
            right.append(Entry.repeat_id != exclude_repeat_id)

        entry = Entry.query.filter(
            or_(and_(*left), and_(*right), and_(*contain))
        ).first()

        return entry


class RepeatService(object):
    ACT_CHECK = "CHECK"
    ACT_EXECUTE = "EXECUTE"

    @classmethod
    def get_by_own(cls,username):
        return Repeat.query.filter_by(create_by=username).all()

    @classmethod
    def get_by_id(cls, id):
        return Repeat.query.filter_by(id=id).first()

    @classmethod
    def is_repeat_day(cls, rep_type):
        return rep_type == Repeat.REP_DAILY

    @classmethod
    def is_repeat_week(cls, rep_type):
        return rep_type == Repeat.REP_WEEKLY

    @classmethod
    def is_repeat_month(cls, rep_type):
        return rep_type == Repeat.REP_MONTHLY

    @classmethod
    def is_repeat(cls, rep_type):
        """ 表示为序列会议"""
        return rep_type in [Repeat.REP_DAILY, Repeat.REP_WEEKLY, Repeat.REP_MONTHLY]

    @classmethod
    def is_check(cls, action):
        """ 判断是否是检测"""
        return action.upper() != cls.ACT_EXECUTE

    @classmethod
    def new(cls, **kwargs):
        dic = {k: v for k, v in kwargs.items() if hasattr(Repeat, k)}
        rep = Repeat(**dic)
        db.session.add(rep)
        db.session.commit()
        return rep

    @classmethod
    def create(
            cls,
            room_id,
            name,
            description,
            start_time,
            end_time,
            end_date,
            rep_type=0,
            rep_opt="0",
            month_absolute=None,
            ical_uid=None,
            ical_sequence=0,
            create_by="",
            modified_by="",
    ):
        """ 创建重复会议
        :param start_time: 开始时间, 时间均为时间戳形式
        :param end_time: 结束时间
        :param end_date: 截止日期
        :param rep_type: 重复类型
        :param rep_opt: 选择的周期, 默认为0 选择周三则 '0001000', 星期日开始
        :param month_absolute: 选择的日期， 重复月是使用
        """
        ical_uid = ical_uid or str(uuid.uuid1())
        rep = Repeat(
            room_id=room_id,
            name=name,
            description=description,
            start_time=start_time,
            end_time=end_time,
            end_date=end_date,
            rep_type=rep_type,
            rep_opt=rep_opt,
            month_absolute=month_absolute,
            ical_uid=ical_uid,
            ical_sequence=ical_sequence,
            create_by=create_by,
            modified_by=modified_by,
            rep_num_weeks=1,
        )
        db.session.add(rep)
        db.session.commit()
        return rep

    @classmethod
    def update_by_id(
            cls,
            repeat_id,
            room_id,
            name,
            description,
            start_time,
            end_time,
            end_date,
            rep_type=0,
            rep_opt="0",
            month_absolute=None,
            modified_by="",
    ):
        """ 编辑会议"""
        rep = cls.get_by_id(repeat_id)
        if not rep:
            return
        ical_uid = rep.ical_uid
        ical_sequence = rep.ical_sequence + 1
        new_rep = cls.create(
            room_id,
            name,
            description,
            start_time,
            end_time,
            end_date,
            rep_type=rep_type,
            rep_opt=rep_opt,
            month_absolute=month_absolute,
            ical_uid=ical_uid,
            ical_sequence=ical_sequence,
            create_by=rep.create_by,
            modified_by=modified_by,
        )
        # 删除原有重复会议
        db.session.delete(rep)
        return new_rep

    @classmethod
    def delete_by_id(cls, repeat_id):
        rep = cls.get_by_id(repeat_id)
        db.session.delete(rep)
        db.session.commit()

    @classmethod
    def get_date_list(cls, start_date, end_date):
        """ 获取重复的日期列表
        :param start_date: 开始日期
        :param end_date: 结束日期
        """
        return get_date_range(start_date, end_date)

    @classmethod
    def get_date_range_by_day(cls, start_time, end_date):
        start_date = start_time.split()[0]
        return get_date_range(start_date, end_date)

    @classmethod
    def get_date_range_by_week(cls, start_time, end_date, rep_week):
        """ 重复的星期
        :param rep_week: 重复的星期
        """
        start_date = start_time.split()[0]
        date_range = get_date_range(start_date, end_date)
        date_range = [date for date in date_range if get_date_week(date) == rep_week]
        return date_range

    @classmethod
    def get_date_range_by_month(cls, start_time, end_date, rep_day):
        """ 重复的日期
        :param repday: 重复的日期
        """
        start_date = start_time.split()[0]
        date_range = get_date_range(start_date, end_date)
        date_range = [date for date in date_range if get_date_day(date) == rep_day]
        return date_range

    @classmethod
    def check_exists(cls, room_id, start_time, end_time, date_range, repeat_id=None):
        """ 判断是否存在冲突
        :param room_id: 会议室
        :param date_rage: 时间范围
        :param repeat_id: 排除repeat_id的会议室
        """
        has_entries, entries = False, []

        st = start_time.split()[1]
        et = end_time.split()[1]

        for date in date_range:
            stime = d_to_t(date + " " + st)
            etime = d_to_t(date + " " + et)
            entry = MeetingService.is_exists(
                room_id, stime, etime, exclude_repeat_id=repeat_id
            )
            if entry:
                has_entries = True
                entries.append(entry)
        return has_entries, entries

    @classmethod
    def create_repeat_meeting(cls, rep, date_range, **kwargs):
        """ 创建重复的会议"""
        entries, exists_entries = [], []

        st = kwargs["start_time"].split()[1]
        et = kwargs["end_time"].split()[1]
        for date in date_range:
            stime = date + " " + st
            etime = date + " " + et
            kw = copy.copy(kwargs)
            kw["start_time"] = stime
            kw["end_time"] = etime
            kw["repeat_id"] = rep.id
            kw["ical_uid"] = rep.ical_uid
            kw["ical_sequence"] = rep.ical_sequence
            status, meeting = MeetingService.new(**kw)
            if status:
                entries.append(meeting)
            else:
                exists_entries.append(meeting)

        return entries, exists_entries

    @classmethod
    def delete_repeat_meeting(cls, repeat_id):
        """ 删除重复的会议"""
        entries = Entry.query.filter_by(repeat_id=repeat_id)
        for entry in entries:
            db.session.delete(entry)

        db.session.commit()

    @classmethod
    def get_repeat_meeting(cls, repeat_id):
        """ 获取重复的会议"""
        return Entry.query.filter_by(repeat_id=repeat_id)

    @classmethod
    def get_repeat_info(cls, repeat_id):
        rep = cls.get_by_id(repeat_id)
        result = dict(
            rep_type=rep.rep_type,
            rep_end_date=rep.end_date,
            month_absolute=rep.month_absolute,
            rep_day=cls.filter_rep_opt(rep.rep_opt),
        )

        return result

    @classmethod
    def filter_rep_opt(cls, rep_opt):
        """ 过滤rep_opt"""
        if not rep_opt:
            return ""
        rep_day = []
        for i, d in enumerate(rep_opt):
            if d == "1":
                rep_day.append(str(i))

        rep_day = "".join(rep_day)
        return rep_day

    @classmethod
    def is_date_after_check_type(cls, start_date, check_date, repType):
        """
        判断截止日期是否超过限制
        :param start_date:开始日期
        :param check_date:截止日期
        :param repType:重复类型
        :return:True or False
        """
        start_date = datetime.datetime.strptime(start_date, '%Y-%m-%d')
        check_date = datetime.datetime.strptime(check_date, '%Y-%m-%d')
        type_dict = {
            0: relativedelta(months=+1),  # REP_NONE
            1: relativedelta(days=+7),  # REP_DAILY   endDate
            2: relativedelta(months=+1),  # REP_WEEKLY
            3: relativedelta(months=+6),  # REP_MONTHLY
        }
        end_date = start_date + type_dict.get(repType)
        if check_date >= end_date:
            return True
        else:
            return False

    @classmethod
    def check_meeting_date(cls, data):
        """判断日期"""
        MESSAGE_DICT = {
            0: "请选择一个月内的日期预定",
            1: "重复日期请选择一周内的日期",
            2: "重复日期请选择一个月内的日期",
            3: "重复日期请选择六个月内的日期",
        }

        repType = int(data['rep_type'])
        start_date = datetime.datetime.strftime(datetime.datetime.now(), "%Y-%m-%d")
        meeting_date = data["start_time"].split(" ")[0]
        # logger.error("first:%s  %s  %s"%(start_date, meeting_date, repType))
        if cls.is_date_after_check_type(start_date, meeting_date, 0):
            return True, MESSAGE_DICT.get(0)

        start_date = meeting_date
        if repType == 0:
            check_date = meeting_date
        else:
            check_date = data["end_date"]
        # logger.error("second:%s  %s  %s" % (start_date, check_date, repType))
        if cls.is_date_after_check_type(start_date, check_date, repType):
            return True, MESSAGE_DICT.get(repType)
        else:
            return False, ""
