# coding=utf-8
# flake8: noqa
import json

from flask import current_app
from flask import g
from flask import request
from sqlalchemy import and_
from sqlalchemy import or_

from models import db
from models import Department
from models import Employee
from models import EntryDepartment
from models import EntryEmployee
from models import Role
from utils import decrypt_token

# noqa: F401


class DepartmentService(object):
    @classmethod
    def get_list(cls, **kw):
        return Department.query.filter_by(**kw)



    @classmethod
    def get_children_by_ids(cls, ids):
        departments = Department.query.filter(
            Department.deleted == 0, Department.parent_id.in_(ids)
        )
        return departments

    @classmethod
    def get_entry_department_ids(cls, entry_id):
        departments = EntryDepartment.query.filter(
            EntryDepartment.deleted == 0, EntryDepartment.entry_id == entry_id
        )
        return [item.department_id for item in departments]

    @classmethod
    def create_entry_departments(cls, entry_id, department_ids):
        """ 创建会议相关部门"""
        for department_id in department_ids:
            entry_department = EntryDepartment(
                entry_id=entry_id, department_id=department_id
            )
            db.session.add(entry_department)
        else:
            db.session.commit()


class EmployeeService(object):

    @classmethod
    def get_usercode_by_create_name(cls,username):
        """通过会议创建人username获取创建人usercode"""
        data = Employee.query.filter(and_(Employee.username==username,Employee.deleted==False)).first()
        if data:
            return data.usercode
        else:
            return ''

    @classmethod
    def current_user(cls):
        """ 当前登陆的用户"""
        token = request.cookies.get("TOKEN")
        test_usercode = current_app.config.get("TEST_USERCODE")
        if test_usercode:
            return cls.get_by_usercode(test_usercode)

        try:
            usercode = decrypt_token(token)["usercode"]
        except:
            return

        return cls.get_by_usercode(usercode)

    @classmethod
    def get_list(cls, **kw):
        return Employee.query.filter_by(**kw)

    @classmethod
    def get_by_usercode(cls, usercode):
        return Employee.query.filter_by(usercode=usercode, deleted=False).first()

    @classmethod
    def get_by_roleid(cls, roleId):
        return Role.query.filter_by(id=roleId).first()

    @classmethod
    def get_list_by_name(cls, name):
        name = u"%{}%".format(name)
        return Employee.query.filter(
            and_(
                Employee.deleted == 0,
                or_(Employee.name.like(name), Employee.username.like(name)),
            )
        )

    @classmethod
    def delete_entry_employees_by_entry_id(cls, entry_id, exclude_usercodes=None):
        """ 删除参会人员
        :param exclude_usercodes 排除的usercode
        """
        if exclude_usercodes is not None:
            delete_entry_employees = EntryEmployee.query.filter(
                EntryEmployee.entry_id == entry_id,
                EntryEmployee.is_owner == 0,
                ~EntryEmployee.usercode.in_(exclude_usercodes),
            )
        else:
            delete_entry_employees = EntryEmployee.query.filter(
                EntryEmployee.entry_id == entry_id, EntryEmployee.is_owner == 0
            )

        usercodes = [item.usercode for item in delete_entry_employees]
        for employee in delete_entry_employees:
            db.session.delete(employee)

        return usercodes

    @classmethod
    def new_entry_employees(
        cls,
        entry_id,
        usercodes,
        current_usercode,
        has_owner=False,
        from_department=False,
    ):
        """ 新增参会人员"""

        if current_usercode in usercodes:
            usercodes.remove(current_usercode)

        for usercode in usercodes:
            entry_employee = EntryEmployee(
                entry_id=entry_id, usercode=usercode, from_department=from_department
            )
            db.session.add(entry_employee)

        # 是否添加当前员工
        if has_owner:
            entry_employee = EntryEmployee(
                entry_id=entry_id,
                usercode=current_usercode,
                is_owner=True,
                status=EntryEmployee.ST_ACCEPT,
                from_department=from_department,
            )
            db.session.add(entry_employee)
        db.session.commit()

    @classmethod
    def get_exists_entry_employees(cls, entry_id, include_usercodes=None):
        """ 获取已有参会人员
        :param usercodes 包含在usercode中
        """
        if include_usercodes is not None:
            exists_entry_employees = EntryEmployee.query.filter(
                EntryEmployee.entry_id == entry_id,
                EntryEmployee.usercode.in_(include_usercodes),
            )
        else:
            exists_entry_employees = EntryEmployee.query.filter(
                EntryEmployee.entry_id == entry_id
            )

        return exists_entry_employees

    @classmethod
    def get_usercodes_by_org(cls, org):
        """ 获取组织下的所有usercode"""
        usercodes = [item["usercode"] for item in org if item["type"] == "EMPLOYEE"]
        department_ids = [item["id"] for item in org if item["type"] == "DEPARTMENT"]
        deparments = DepartmentService.get_children_by_ids(department_ids)
        department_ids.extend([item.id for item in deparments])
        department_ids = list(set(department_ids))

        employees = Employee.query.filter(
            Employee.deleted == 0, Employee.department_id.in_(department_ids)
        )
        usercodes.extend([item.usercode for item in employees])
        return list(set(usercodes))

    @classmethod
    def create_entry_employees(cls, entry_id, org, current_usercode):
        """ 创建参会人员"""
        department_ids = [item["id"] for item in org if item["type"] == "DEPARTMENT"]
        # 创建会议部门
        DepartmentService.create_entry_departments(entry_id, department_ids)

        # 获取子部门
        deparments = DepartmentService.get_children_by_ids(department_ids)
        department_ids.extend([item.id for item in deparments])
        department_ids = list(set(department_ids))

        department_employees = Employee.query.filter(
            Employee.deleted == 0, Employee.department_id.in_(department_ids)
        )
        department_employee_usercodes = [item.usercode for item in department_employees]

        # 创建会议部门成员
        cls.new_entry_employees(
            entry_id,
            department_employee_usercodes,
            current_usercode,
            from_department=True,
        )

        usercodes = [item["usercode"] for item in org if item["type"] == "EMPLOYEE"]
        usercodes = [
            item for item in usercodes if item not in department_employee_usercodes
        ]
        usercodes = list(set(usercodes))
        # 创建会议成员
        cls.new_entry_employees(entry_id, usercodes, current_usercode, has_owner=True)

    @classmethod
    def delete_entry_employees(cls, entry_id, org):
        """ 删除参会人员及部门
        :param entry_id: 会议id
        :param org: 结构
        """
        department_ids = [item["id"] for item in org if item["type"] == "DEPARTMENT"]

        departments = EntryDepartment.query.filter(
            EntryDepartment.entry_id == entry_id,
            ~EntryDepartment.department_id.in_(department_ids),
        )

        for department in departments:
            db.session.delete(department)

        usercodes = cls.get_usercodes_by_org(org)
        return cls.delete_entry_employees_by_entry_id(entry_id, usercodes)

    @classmethod
    def create_entry_departments(cls, entry_id, org):
        """ 创建参会部门"""
        department_ids = [item["id"] for item in org if item["type"] == "DEPARTMENT"]
        exists_department_ids = DepartmentService.get_entry_department_ids(entry_id)
        new_department_ids = [
            item for item in department_ids if item not in exists_department_ids
        ]

        DepartmentService.create_entry_departments(entry_id, new_department_ids)

    @classmethod
    def get_deleted_employee_list(cls):
        """获取离职人员"""
        return Employee.query.filter_by(deleted=True).all()