# coding=utf-8
from sqlalchemy import and_
from sqlalchemy import or_

from models import Area
from models import Device
from models import Entry
from models import Room
from models import RoomDevice
from utils import datetime_str_to_timestamp as d_to_t

# noqa: E501


def get_start_end_stamp(date):
    """ 开始结束时间的时间戳"""
    st = "{} 00:00".format(date)
    et = "{} 23:59".format(date)
    return d_to_t(st), d_to_t(et)


def handle_key(*args, **kwargs):
    item = args[1]
    return str(item.id)


def format_device(room_id):
    device_list_obj = RoomDevice.query.filter_by(room_id=room_id).all()
    device_id_list = [item.device_id for item in device_list_obj]
    device_list = []
    for id in device_id_list:
        device_list.append((Device.query.filter_by(id=id).first()).name)
    return device_list


class AreaService(object):
    @classmethod
    def get_list(cls):
        return Area.query.filter_by(disabled=Area.ST_ENABLE)


class RoomService(object):

    AREA_START_HOUR = 8
    AREA_END_HOUR = 21

    @classmethod
    def get_list_by_area_id(cls, area_id):
        """ 根据区域id返回房间"""
        room_list = Room.query.filter_by(area_id=area_id, disabled=Room.ST_ENABLE)
        return room_list

    @classmethod
    def get_area_time_frame(cls, area):
        """ 获取时段"""
        frames = []
        start_hour = area.morningstarts or cls.AREA_START_HOUR
        end_hour = area.eveningends or cls.AREA_END_HOUR
        for hour in range(start_hour, end_hour + 1):
            frames.append("{hour}:00".format(hour=hour))
            frames.append("{hour}:30".format(hour=hour))
        return frames

    @classmethod
    def get_room_device_by_room_id(cls, room_id):
        """根据会议室id获取会议室设备"""
        device_list = RoomDevice.query.filter_by(room_id=room_id)
        device_id_list = []
        for device in device_list:
            device_id_list.append(device.device_id)
        devices = Device.query.filter(
            Device.id.in_(device_id_list)
        )
        room_device_list = []
        for device in devices:
            device = {
                "id": device.id,
                "name": device.name
            }
            room_device_list.append(device)
        return room_device_list

    @classmethod
    def get_area_room_list(cls, area_name, date):
        """ 根据区域房间信息"""
        area = Area.query.filter_by(area_name=area_name).first()
        frames = cls.get_area_time_frame(area)
        room_list = cls.get_list_by_area_id(area.id)
        room_id_list = [room.id for room in room_list]

        st, et = get_start_end_stamp(date)

        entry_list = Entry.query.filter(
            Entry.room_id.in_(room_id_list),
            or_(
                and_(Entry.start_time > st, Entry.start_time < et),
                and_(Entry.end_time > st, Entry.end_time < et),
                and_(Entry.start_time < st, Entry.end_time > et),
            ),
        )

        # 房间会议列表
        entry_map = {}
        for entry in entry_list:
            room_id = entry.room_id
            dic = {"st": entry.start_time, "et": entry.end_time, "id": entry.id}
            if room_id not in entry_map:
                entry_map[room_id] = [dic]
            else:
                entry_map[room_id].append(dic)

        # 添加时间片段
        room_list = [
            {
                "id": room.id,
                "name": room.room_name,
                "capacity": room.capacity,
                "frames": {frame: {"status": "N"} for frame in frames},
                "startHour": area.morningstarts or cls.AREA_START_HOUR,
                "endHour": area.eveningends or cls.AREA_END_HOUR,
                "devices": cls.get_room_device_by_room_id(room_id=room.id),
                "device": format_device(room.id),
            }
            for room in room_list
        ]

        # 循环时间片段， 查看该时间内是否存在会议
        for frame in frames:
            frame_time = "{date} {frame}".format(date=date, frame=frame)
            frame_timestamp = d_to_t(frame_time)
            for room in room_list:
                # 某个房间的当天所有会议列表
                room_entry_list = entry_map.get(room["id"]) or []
                for room_entry in room_entry_list:
                    if (
                        room_entry["st"] <= frame_timestamp
                        and room_entry["et"] > frame_timestamp
                    ):
                        room["frames"][frame]["status"] = "E"  # exists
                        room["frames"][frame]["id"] = room_entry["id"]  # exists
        return room_list

    @classmethod
    def get_area_room_by_room_id(cls, room_id, date):
        room = Room.query.filter_by(id=room_id).first()
        area = Area.query.filter_by(id=room.area_id).first()
        frames = cls.get_area_time_frame(area)
        room_device_list = cls.get_room_device_by_room_id(room_id=room_id)

        st, et = get_start_end_stamp(date)
        entry_list = Entry.query.filter(
            and_(
                and_(
                    Entry.room_id == room_id,
                ),
                or_(
                    and_(Entry.start_time > st, Entry.start_time < et),
                    and_(Entry.end_time > st, Entry.end_time < et),
                    and_(Entry.start_time < st, Entry.end_time > et),
                ),
            ),

        )

        entry_list = [
            {"st": entry.start_time, "et": entry.end_time, "id": entry.id, "repeatId": entry.repeat_id or ''}
            for entry in entry_list
        ]

        room = {
            "id": room.id,
            "name": room.room_name,
            "capacity": room.capacity,
            "frames": {frame: {"status": "N"} for frame in frames},
            "startHour": area.morningstarts or cls.AREA_START_HOUR,
            "endHour": area.eveningends or cls.AREA_END_HOUR,
            "devices": room_device_list,
        }

        for frame in frames:
            frame_time = "{date} {frame}".format(date=date, frame=frame)
            frame_timestamp = d_to_t(frame_time)
            for entry in entry_list:
                if entry["st"] <= frame_timestamp and entry["et"] > frame_timestamp:
                    room["frames"][frame]["status"] = "E"  # exists
                    room["frames"][frame]["id"] = entry["id"]
                    room["frames"][frame]['repeatId'] = entry["repeatId"]
        return room

    @classmethod
    def get_devices(cls):
        device_list = Device.query.filter_by()
        deviceList = []
        for device in device_list:
            device = {
                "id": device.id,
                "name": device.name
            }
            deviceList.append(device)
        return deviceList
