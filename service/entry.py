# coding=utf-8
import uuid

from models import db
from models import Employee
from models import Entry
from models import EntryEmployee


def make_ical_uid():
    """ 生成唯一的id"""
    return str(uuid.uuid1())


class EntryService(object):
    """ 会议服务"""

    @classmethod
    def get_by_id(cls, id):
        return Entry.query.filter_by(id=id).first()

    @classmethod
    def create(cls, room_id, name, descripiton,
               start_time, end_time, create_by,
               repeat_id=None, ical_uid=None, ical_sequence=None, **kw):
        """ 新建会议"""
        entry = Entry(
            room_id=room_id, name=name,
            descripiton=descripiton, start_time=start_time,
            end_time=end_time, create_by=create_by,
            ical_uid=ical_uid or make_ical_uid(), ical_sequence=ical_sequence or 0,
            repeat_id=repeat_id or 0,
        )
        db.session.add(entry)
        db.session.commit()
        return entry

    @classmethod
    def get_owner(cls, entry_id):
        """ 获取会议预订者信息"""
        querys = db.session.query(EntryEmployee, Employee).filter(
            EntryEmployee.entry_id == entry_id,
            EntryEmployee.usercode == Employee.usercode,
            EntryEmployee.is_owner == 1,
        ).first()
        return querys

    @classmethod
    def get_info(cls, id):
        """ 获取信息"""
        querys = db.session.query(Entry, EntryEmployee, Employee).filter(
            Entry.id == EntryEmployee.entry_id,
            EntryEmployee.usercode == Employee.usercode,
            EntryEmployee.is_owner == 1,
            EntryEmployee.entry_id == id,
        ).first()
        return querys


class RepEntryService(object):
    """ 重复会议服务"""

    pass
