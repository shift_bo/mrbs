# coding=utf-8
from collections import namedtuple

from service.ext.weixin import WXService
from utils import timestamp_to_datetime_str


NoticeInfo = namedtuple('NoticeInfo', ['title', 'info'])


def make_notice_description(entry, info):
    """ 通知描述"""
    startTime = int(entry["startTime"] / 1000)
    startTime = timestamp_to_datetime_str(startTime, fmt="%m月%d日 %H:%M").decode("utf8")
    return u"""<div>{notice_info}\n会议时间: {notice_time}\n会议地点: {notice_location}\n会议主题: {notice_topic}</div>""".format(
        notice_info=info,
        notice_time=startTime,
        notice_location=entry["location"],
        notice_topic=entry["name"],
    )


class NoticeService(object):

    NEW = NoticeInfo(title=u'成功预定', info=u'您已成功预定会议')
    EDIT = NoticeInfo(title=u'会议变更', info=u'会议信息变更，请您尽快确认哦!')
    CANCEL = NoticeInfo(title=u'会议取消', info=u'您要参加的会议已取消!')
    INVITE = NoticeInfo(title=u'会议邀请', info=u'{owner}邀请您参加会议， 请您尽快确认哦!')
    BEFORE = NoticeInfo(title=u'会前提醒', info=u'您要参加的会议马上就要开始了!')

    @classmethod
    def get_notice_info(cls, entry, notice_info, owner=None):
        """ 通知信息"""
        title, info = notice_info.title, notice_info.info
        if owner:
            info = info.format(owner=owner['name'])
        description = make_notice_description(entry, info)
        return title, description

    @classmethod
    def new_message(cls, entry):
        """ 新增会议"""
        owner = entry["owner"]
        title, description = cls.get_notice_info(entry, cls.NEW)
        WXService.send_notice(title, description, touser=owner['usercode'], id=entry['id'])

    @classmethod
    def edit_message(cls, entry, usercodes):
        """ 会议变成"""
        if not usercodes:
            return

        title, description = cls.get_notice_info(entry, cls.EDIT)
        WXService.send_notice(title, description, touser="|".join(usercodes), id=entry['id'])

    @classmethod
    def cancel_message(cls, entry, usercodes):
        """ 取消会议"""
        if not usercodes:
            return
        title, description = cls.get_notice_info(entry, cls.CANCEL)
        WXService.send_notice(title, description, touser="|".join(usercodes))

    @classmethod
    def invite_message(cls, entry, usercodes):
        """ 会议邀请"""
        if not usercodes:
            return
        owner = entry['owner']
        title, description = cls.get_notice_info(entry, cls.INVITE, owner=owner)
        WXService.send_notice(title, description, touser="|".join(usercodes), id=entry['id'])

    @classmethod
    def before_message(cls, entry):
        """ 会前提醒"""
        employees = entry["employees"]
        usercodes = [employee["usercode"] for employee in employees]
        title, description = cls.get_notice_info(entry, cls.BEFORE)
        WXService.send_notice(title, description, touser="|".join(usercodes), id=entry['id'])
