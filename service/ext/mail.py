# coding=utf-8
import requests
from flask import current_app

from logger import logger


class MailService(object):
    """ 邮件服务"""

    @classmethod
    def send_canlendar_mail(cls, **kw):
        mail_host = current_app.config.get("MAIL_HOST")
        url = "http://{mail_host}/api/users/invite".format(mail_host=mail_host)
        try:
            requests.post(url, data=kw, timeout=3)
        except Exception as e:
            logger.error(str(e))
