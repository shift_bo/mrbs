# coding=utf-8
import json
import time
import urllib

import requests
from flask import current_app

from logger import logger
from utils import make_rnd_text
from utils import make_sha1_hash
from utils.decorators import cached


class WXService(object):
    @classmethod
    def request(cls, url, method="GET", params=None, data=None):
        try:
            response = requests.request(
                method, url, params=params, data=data, allow_redirects=False
            )
            if response.status_code == 200:
                return response.json()
            else:
                return {}
        except Exception as e:
            logger.error(e.message)
            return {}

    @classmethod
    def _parse_data(cls, data, errname):
        if data.get("errcode") == 0:
            return data
        elif data.get("errcode") != 0:
            logger.error(
                "{errname}({errcode}): {errmsg}".format(
                    errname=errname, errcode=data["errcode"], errmsg=data["errmsg"]
                )
            )
        return {}

    @classmethod
    @cached(expires=7000)
    def get_access_token(cls):
        """ 获取access_token"""
        url = current_app.config.get("WX_ACCESS_TOKEN_URL")
        data = cls.request(url)
        data = cls._parse_data(data, "AccessTokenError")
        return data.get("access_token", "")

    @classmethod
    def make_signature(cls, jsapi_ticket=None, noncestr=None, timestamp=None, url=None):
        """ 生成签名"""
        text = "jsapi_ticket={}&noncestr={}&timestamp={}&url={}".format(
            jsapi_ticket, noncestr, timestamp, url
        )
        return make_sha1_hash(text)

    @classmethod
    @cached(expires=7000)
    def get_jsapi_ticket(cls):
        """ 获取企业jsapi_ticket"""
        url = current_app.config.get("WX_JSAPI_TICKET_URL").format(
            access_token=cls.get_access_token()
        )
        data = cls.request(url)
        data = cls._parse_data(data, "JSAPITicketError")
        return data.get("ticket", "")

    @classmethod
    @cached(expires=7000)
    def get_app_jsapi_ticket(cls):
        """ 获取应用jsapi_ticket"""
        url = current_app.config.get("WX_APP_JSAPI_TICKET_URL").format(
            access_token=cls.get_access_token()
        )
        data = cls.request(url)
        data = cls._parse_data(data, "APPJSAPITicketError")
        return data.get("ticket", "")

    @classmethod
    def get_jssdk_config(cls, url):
        """ 获取jssdk配置"""
        jsapi_ticket = cls.get_jsapi_ticket()
        timestamp = int(time.time())
        noncestr = make_rnd_text(16)
        signature = cls.make_signature(
            jsapi_ticket=jsapi_ticket, noncestr=noncestr, timestamp=timestamp, url=url
        )
        config = {
            "appid": current_app.config.get("WX_CORP_ID"),
            "timestamp": timestamp,
            "noncestr": noncestr,
            "signature": signature,
        }
        return config

    @classmethod
    def make_web_oauth2_link(cls, redirect_uri=None, state=""):
        """ 构造企业网页授权oauth2链接
        :param scope: 应用授权作用于
        """
        scope = "snsapi_base"
        redirect_uri = urllib.quote(redirect_uri)
        corp_oauth2_url = current_app.config.get("WX_CORP_OAUTH2_URL")
        appid = current_app.config.get("WX_CORP_ID")
        agentid = current_app.config.get("WX_AGENT_ID")
        url = corp_oauth2_url.format(
            corpid=appid,
            agentid=agentid,
            scope=scope,
            redirect_uri=redirect_uri,
            state=state,
        )
        return url

    @classmethod
    def get_visitor_info(cls, code):
        """ 获取访问者用户身份"""
        url = "https://qyapi.weixin.qq.com/cgi-bin/user/getuserinfo?access_token={access_token}&code={code}"
        url = url.format(access_token=cls.get_access_token(), code=code)
        data = cls.request(url)
        data = cls._parse_data(data, "GetVisitorInfo")
        return data.get("UserId", "")

    @classmethod
    def get_user_info(cls, usercode):
        """ 获取用户信息"""
        url = "https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token={access_token}&userid={user_id}"
        url = url.format(access_token=cls.get_access_token(), user_id=usercode)
        data = cls.request(url)
        data = cls._parse_data(data, "GetUserInfo")
        return data

    @classmethod
    def get_app_info(cls):
        """ 获取应用详情"""
        agentid = current_app.config.get("WX_AGENT_ID")
        url = "https://qyapi.weixin.qq.com/cgi-bin/agent/get?access_token={access_token}&agentid={agentid}"
        url = url.format(access_token=cls.get_access_token(), agentid=agentid)
        data = cls.request(url)
        data = cls._parse_data(data, "GetAppInfo")
        return data

    @classmethod
    def send_message(cls, message):
        """ 发送应用信息"""
        url = "https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token={access_token}"
        url = url.format(access_token=cls.get_access_token())

        data = cls.request(url, method="POST", data=message)
        data = cls._parse_data(data, "SendMessage")
        return data

    @classmethod
    def send_notice(cls, title, description, touser='', toparty='', **kw):
        """ 发送通知
        :param touser: 2915|xxxx|yyyy 最多支持1000个, @all，则向关注该企业应用的全部成员发送
        :param toparty: 2915|xxxx|yyyy 最多支持100个
        :param title: 标题
        :param description: 具体内容
        """

        mid = kw.get('id') or ''

        logger.info('[Notice] [Entry: {}] toUser: {}'.format(mid, touser))

        if "id" in kw:
            url = "http://mrbs.17zuoye.net/#/?mid={id}".format(id=mid)
        else:
            url = "http://mrbs.17zuoye.net/#/"

        data = {
            "touser": touser,
            'toparty': toparty,
            "msgtype": "textcard",
            "agentid": current_app.config.get("WX_AGENT_ID"),
            "textcard": {
                "title": title,
                "description": description,
                "url": url,
                "btntxt": "详情",
            },
        }
        resp_data = cls.send_message(json.dumps(data))
        invaliduser = resp_data.pop('invaliduser', None)
        invalidparty = resp_data.pop('invalidparty', None)
        if invaliduser:
            logger.error('[Notice][Entry: {}] [Invaliduser] {}'.format(mid, invaliduser))
        if invalidparty:
            logger.error('[Notice][Entry: {}] [invalidparty] {}'.format(mid, invalidparty))
        return data

    @classmethod
    def get_department_member(cls, department_id, fetch_child="0"):
        """获取部门成员"""
        url = current_app.config.get("WX_DEPARTMENT_MEMBER_URL").format(
            access_token=cls.get_access_token(),
            department_id=department_id,
            fetch_child=fetch_child,
        )
        data = cls.request(url)
        data = cls._parse_data(data, "GetDepartmentMember<dep_id: {}>".format(department_id))
        return data.get("userlist") or []

    @classmethod
    def get_department_member_detail(cls, department_id, fetch_child="0"):
        """ 获取部门成员详情"""
        url = current_app.config.get("WX_DEPARTMENT_MEMBER_DETAIL_URL").format(
            access_token=cls.get_access_token(),
            department_id=department_id,
            fetch_child=fetch_child,
        )
        data = cls.request(url)
        data = cls._parse_data(data, "GetDepartmentMemberDetail<dep_id: {}>".format(department_id))
        return data.get("userlist") or []

    @classmethod
    def get_department_list(cls):
        """获取部门列表"""
        url = current_app.config.get("WX_DEPARTMENT_LIST_URL").format(
            access_token=cls.get_access_token()
        )
        data = cls.request(url)
        data = cls._parse_data(data, "GetDepartmentList")
        return data.get("department") or []
