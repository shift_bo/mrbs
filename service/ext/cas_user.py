# coding=utf-8
from flask import current_app
from vox_consul import Consul


class CASUserService(object):

    _URL = "auge.service.cas.user"

    @classmethod
    def getClient(cls):
        HOST = current_app.config.get("VOX_CONSUL_HOST")
        DC = current_app.config.get("VOX_CONSUL_DC")
        consul = Consul(host=HOST, dc=DC)
        consul._configure()
        consul.service.register()
        return consul

    @classmethod
    def request(cls, api_url, params=None, method="GET", timeout=3):
        client = cls.getClient()
        try:
            data = client.request.do_action_without_invalid_error(
                cls._URL,
                api_url,
                params=params,
                method=method,
                headers={"Content-Type": "application/x-www-form-urlencoded"},
                timeout=3,
            )
            return data
        except Exception:
            return {}

    @classmethod
    def _parse_data(cls, data):
        if data.get("status") != 0:
            return False, data["message"]
        return True, data["data"]

    @classmethod
    def get_user_info_by_usercode(cls, usercode, params=None):
        data = cls.request("/api/v1/user/usercode/{}".format(usercode), params=params)
        return cls._parse_data(data)
