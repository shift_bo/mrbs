# coding=utf-8
from celery.schedules import crontab

DEBUG = True

SITE = "http://mrbs.17zuoye.net"
SECRET_KEY = "MeetingRoomBookingSystem"
SESSION_COOKIE_HTTPONLY = False

# MEETING_PLUG
MAIL_HOST = "192.168.100.53:7433"


# WEIXIN
WX_CORP_ID = "ww496415a282ed8602"
WX_AGENT_ID = "1000030"
WX_CORP_SECRET = "1soGIrvzkXAXJFASDWFLLzfGFyjXV2fFfGjlhQ89z0o"
WX_ACCESS_TOKEN_URL = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid={corpid}&corpsecret={corpsecret}".format(
    corpid=WX_CORP_ID, corpsecret=WX_CORP_SECRET
)
WX_JSAPI_TICKET_URL = (
    "https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket?access_token={access_token}"
)
WX_APP_JSAPI_TICKET_URL = "https://qyapi.weixin.qq.com/cgi-bin/ticket/get?access_token={access_token}&type=agent_config"
WX_CORP_OAUTH2_URL = "https://open.weixin.qq.com/connect/oauth2/authorize?appid={corpid}&redirect_uri={redirect_uri}&response_type=code&scope={scope}&agentid={agentid}&state={state}#wechat_redirect"
WX_OAUTH2_CALLBACK = SITE + "/api/v1/weixin/oauth2/callback"
WX_DEPARTMENT_MEMBER_URL = "https://qyapi.weixin.qq.com/cgi-bin/user/simplelist?access_token={access_token}&department_id={department_id}&fetch_child={fetch_child}"
WX_DEPARTMENT_MEMBER_DETAIL_URL = "https://qyapi.weixin.qq.com/cgi-bin/user/list?access_token={access_token}&department_id={department_id}&fetch_child={fetch_child}"
WX_DEPARTMENT_LIST_URL = (
    "https://qyapi.weixin.qq.com/cgi-bin/department/list?access_token={access_token}"
)

# REDIS
REDIS_URL = "redis://localhost:6379/0"

CELERY_BROKER_URL = "redis://localhost:6379/0"
CELERY_RESULT_BACKEND = "redis://localhost:6379/1"
CELERY_IMPORTS = ["tasks.employee", "tasks.notice"]

CELERYBEAT_SCHEDULE = {
    "before-meeting-notice": {
        "task": "tasks.notice.before_meeting_notice",
        "schedule": crontab(minute="*/5"),
    },
    "before-repeat-meeting-notice": {
        "task": "tasks.notice.before_repeat_meeting_notice",
        "schedule": crontab(hour=20, minute=5),
    },
    "release_del_employee_meeting":{
        "task":"tasks.notice.release_del_employee_meeting",
        "schedule": crontab(hour=20,minute=30)
    },
    "sync": {"task": "tasks.employee.sync", "schedule": crontab(minute="*/30")},
}
# consul
VOX_CONSUL_HOST = "10.7.11.196"
VOX_CONSUL_DC = "ops"

# DB
DB_CONFIG = dict(host="127.0.0.1", db="mrbs", username="root", password="81486125")
DB_URI = "mysql+pymysql://{}:{}@{}/{}?charset=utf8".format(
    DB_CONFIG["username"], DB_CONFIG["password"], DB_CONFIG["host"], DB_CONFIG["db"]
)
SQLALCHEMY_DATABASE_URI = DB_URI
SQLALCHEMY_POOL_RECYCLE = 600
SQLALCHEMY_TRACK_MODIFICATIONS = True

# 测试模拟登录用户
TEST_USERCODE = "6003"
# BEFORE_MEETING_NOTICE
BEFORE_MEETING_NOTICE_MINUTE = "10"
