# coding=utf-8
from logger import logger
from models import db
from models import Department
from models import Employee
from models import Role
from service.ext.weixin import WXService
from worker import celery_app


@celery_app.task
def sync_employee():
    app_info = WXService.get_app_info()
    if not app_info:
        return

    users = app_info["allow_userinfos"]["user"]
    usercodes = [item["userid"] for item in users]

    role = Role.query.filter_by(name="MEMBER").first()

    # 判断并添加用户
    for usercode in usercodes:
        employee = Employee.query.filter_by(usercode=usercode).first()
        if not employee:
            user_info = WXService.get_user_info(usercode)
            if not user_info:
                continue
            update_employee(user_info, role_id=role.id)
        elif employee.deleted:
            employee.deleted = False

        db.session.commit()


def update_employee(member, department_id=None, role_id=None):
    """ 更新成员
    :param member: 成员信息
    :param department_id: 部门id
    :param role_id: 角色id
    """

    if not member["email"]:
        logger.error('MemberNotFoundEmail: {}, {}'.format(member['userid'], member['name'].encode('utf8')))
        return

    if not department_id:
        department_id = member["department"][-1] if member["department"] else 0

    if not role_id:
        role = Role.query.filter_by(name="MEMBER").first()
        role_id = role.id

    username = member["email"].split("@")[0]
    avatar = member["avatar"][:-1] + "100"

    try:
        employee = Employee.query.filter_by(usercode=member["userid"]).first()
        if not employee:
            employee = Employee(
                usercode=member["userid"],
                email=member["email"],
                name=member["name"],
                username=username,
                avatar=avatar,
                role_id=role_id,
                department_id=department_id,
            )
            db.session.add(employee)
            logger.info('AddEmployee: {}'.format(member['userid']))
        else:
            employee.email = member['email']
            employee.name = member['name']
            employee.username = username
            employee.avatar = avatar
            employee.department_id = department_id
            if employee.deleted:
                employee.deleted = False
            logger.info('UpdateEmployee: {}, {}'.format(employee.usercode, employee.name.encode('utf8')))
    except Exception as e:
        logger.error(str(e).encode('utf8'))


def update_department_employee(department_id):
    """ 更新部门成员
    :param department_id: 更新的部门id
    """
    logger.info('GetDepartmentMembers: {}'.format(department_id))
    members = WXService.get_department_member_detail(department_id)

    if not members:
        return

    role = Role.query.filter_by(name="MEMBER").first()
    for member in members:
        update_employee(member, department_id=department_id, role_id=role.id)
    else:
        db.session.commit()


def update_delete_employee():
    """ 更新删除的人员"""
    app_info = WXService.get_app_info()
    if app_info:
        users = app_info["allow_userinfos"]["user"]
        usercodes = [item["userid"] for item in users]
    else:
        usercodes = []

    departments = Department.query.all()
    for department in departments:
        members = WXService.get_department_member_detail(department.id)
        usercodes.extend([member["userid"] for member in members])

    # 现有的微信成员架构
    usercodes = list(set(usercodes))

    # 删除用户
    employees = Employee.query.filter(
        Employee.deleted == 0, ~Employee.usercode.in_(usercodes)
    )
    for employee in employees:
        employee.deleted = True
    else:
        db.session.commit()


@celery_app.task
def sync_department():
    """ 更新部门"""
    departments = WXService.get_department_list()
    if not departments:
        return

    department_ids = [item["id"] for item in departments]

    for department in departments:
        dep = Department.query.filter_by(id=department["id"]).first()

        if department["parentid"] in department_ids:
            parent_id = department["parentid"]
        else:
            parent_id = 0

        if not dep:
            dep = Department(
                id=department["id"],
                name=department["name"],
                parent_id=parent_id,
                order=department["order"],
            )
            db.session.add(dep)
        else:
            if dep.name != department["name"]:
                dep.name = department["name"]

            if dep.parent_id != parent_id:
                dep.parent_id = parent_id

            if dep.order != department["order"]:
                dep.order = department["order"]

        db.session.commit()

        # 更新部门下的成员
        update_department_employee(dep.id)


@celery_app.task
def sync():
    sync_department()
    sync_employee()
    update_delete_employee()
