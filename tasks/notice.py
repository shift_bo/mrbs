# coding=utf-8
import datetime
import json
import time

from flask import current_app

from models import Entry
from models import EntryNotice
from service.employee import EmployeeService
from service.ext.mail import MailService
from service.ext.notice import NoticeService
from service.ext.weixin import WXService
from service.meeting import MeetingService
from service.meeting import RepeatService
from utils import datetime_str_to_timestamp
from utils import timestamp_to_datetime_str
from worker import celery_app


def make_notice_description(meeting, info):
    """ 通知描述"""
    startTime = int(meeting["startTime"] / 1000)
    startTime = timestamp_to_datetime_str(startTime, fmt="%m月%d日 %H:%M").decode("utf8")
    return u"""<div>{notice_info}\n会议时间: {notice_time}\n会议地点: {notice_location}\n会议主题: {notice_topic}</div>""".format(
        notice_info=info,
        notice_time=startTime,
        notice_location=meeting["location"],
        notice_topic=meeting["name"],
    )


@celery_app.task
def send_notice_by_meeting(meeting_id, org, current_usercode):
    """ 发送通知(一般会议)
    :param meeting_id: 会议id
    :param org: 人员部门结构
    """
    usercodes = EmployeeService.get_usercodes_by_org(org)

    # 删除的参会人员
    delete_usercodes = EmployeeService.delete_entry_employees(meeting_id, org)
    EmployeeService.create_entry_departments(meeting_id, org)

    # 已存在的参会人员
    exists_employees = EmployeeService.get_exists_entry_employees(meeting_id)
    exists_usercodes = [item.usercode for item in exists_employees]

    # 新增的参会人员
    new_usercodes = [
        usercode for usercode in usercodes if usercode not in exists_usercodes
    ]
    EmployeeService.new_entry_employees(
        meeting_id, new_usercodes, current_usercode, has_owner=False
    )

    # 发送会议通知
    meeting_info = MeetingService.get_entry_info(MeetingService.get_by_id(meeting_id))
    send_cancel_message.delay(meeting_info, delete_usercodes)
    send_edit_message.delay(meeting_info, exists_usercodes)
    send_invite_message.delay(meeting_info, new_usercodes)


@celery_app.task
def send_notice_by_repeat(repeat_id, org, current_usercode, skip_list=None):
    """ 发送通知(重复会议)
    :param repeat_id: 重复id
    :param org: 人员部门结构
    :param skip_list: 跳过的列表
    """

    usercodes = EmployeeService.get_usercodes_by_org(org)
    meetings = RepeatService.get_repeat_meeting(repeat_id)

    for meeting in meetings:
        # 删除的参会人员
        delete_usercodes = EmployeeService.delete_entry_employees(meeting.id, org)
        EmployeeService.create_entry_departments(meeting.id, org)

        # 已存在的参会人员
        exists_employees = EmployeeService.get_exists_entry_employees(meeting.id)
        exists_usercodes = [item.usercode for item in exists_employees]

        # 新增的参会人员
        new_usercodes = [
            usercode for usercode in usercodes if usercode not in exists_usercodes
        ]

        EmployeeService.new_entry_employees(
            meeting.id, new_usercodes, current_usercode, has_owner=False
        )

    meeting = meetings.first()
    # 发送会议通知
    meeting_info = MeetingService.get_entry_info(MeetingService.get_by_id(meeting.id))

    repeat_info = RepeatService.get_repeat_info(repeat_id)
    repeat_info["skip_list"] = skip_list or []

    send_cancel_message.delay(meeting_info, delete_usercodes)
    send_edit_message.delay(meeting_info, exists_usercodes, repeat_info=repeat_info)
    send_invite_message.delay(meeting_info, new_usercodes, repeat_info=repeat_info)


@celery_app.task
def send_new_message(meeting, repeat_info=None):
    """ 新增会议"""
    employees = meeting['employees']
    usercodes = [
        employee["usercode"] for employee in employees if not employee["isOwner"]
    ]
    NoticeService.new_message(meeting)
    # 发送邀请
    send_invite_message(meeting, usercodes, is_new=True, repeat_info=repeat_info)


@celery_app.task
def send_edit_message(meeting, usercodes, repeat_info=None):
    """ 会议变更"""
    if not usercodes:
        return
    NoticeService.edit_message(meeting, usercodes)
    title, description = NoticeService.get_notice_info(meeting, NoticeService.EDIT)
    _title = u"{info}: {name}".format(info=title, name=meeting["name"])
    send_notice_mail(_title, description, meeting, repeat_info=repeat_info)


@celery_app.task
def send_cancel_message(meeting, usercodes):
    """ 会议取消"""
    if not usercodes:
        return
    NoticeService.cancel_message(meeting, usercodes)
    title, description = NoticeService.get_notice_info(meeting, NoticeService.CANCEL)

    _title = u"{info}: {name}".format(info=title, name=meeting["name"])
    send_notice_mail(_title, description, meeting, method="CANCEL")


@celery_app.task
def send_invite_message(meeting, usercodes, is_new=False, repeat_info=None):
    """ 会议邀请"""

    owner = meeting['owner']
    NoticeService.invite_message(meeting, usercodes)
    if is_new:
        usercodes.append(owner["usercode"])

    title, description = NoticeService.get_notice_info(meeting, NoticeService.INVITE, owner=owner)
    _title = u"{info}: {name}".format(info=title, name=meeting["name"])
    send_notice_mail(
        _title, description, meeting, usercodes=usercodes, repeat_info=repeat_info
    )


@celery_app.task
def send_before_meeting_message(meeting):
    """ 会前提醒"""
    NoticeService.before_message(meeting)


@celery_app.task
def send_before_repeat_meeting_message(meeting):
    """ 重复会议消息"""
    employees = meeting["employees"]
    usercodes = [employee["usercode"] for employee in employees]
    owner = meeting["owner"]
    title, info = u"会议邀请", u"{owner}邀请您参加会议， 请您尽快确认哦!".format(owner=owner["name"])
    description = make_notice_description(meeting, info)
    WXService.send_notice("|".join(usercodes), title, description)


@celery_app.task
def before_meeting_notice():
    """ 会前提醒，通过配置文件获取检查间隔时间，进行检查一次"""
    minute = int(current_app.config.get("BEFORE_MEETING_NOTICE_MINUTE"))
    st = int(time.time())
    et = st + 60 * minute
    entrys = Entry.query.filter(
        Entry.start_time > st, Entry.start_time <= et
    )

    for entry in entrys:
        entry_info = MeetingService.get_entry_info(entry)
        if entry_info.get('employees'):
            send_before_meeting_message(entry_info)


@celery_app.task
def before_repeat_meeting_notice():
    """ 重复预定的会议， 会在每天晚上8点进行消息提醒"""
    dt = datetime.datetime.now() + datetime.timedelta(days=1)
    start_date = datetime.datetime(dt.year, dt.month, dt.day, 0, 0, 0)
    end_date = datetime.datetime(dt.year, dt.month, dt.day, 23, 59, 59)

    fmt = "%Y%m%d %H:%M"

    st = datetime_str_to_timestamp(start_date.strftime(fmt), fmt=fmt)
    et = datetime_str_to_timestamp(end_date.strftime(fmt), fmt=fmt)
    entries = Entry.query.filter(
        Entry.start_time > st, Entry.end_time <= et
    )

    for entry in entries:
        entry_info = MeetingService.get_entry_info(entry)
        if entry_info.get('employees'):
            send_before_repeat_meeting_message(entry_info)


def send_notice_mail(
    subject, description, meeting, usercodes=None, method="REQUEST", repeat_info=None
):
    """ 发送通知邮件
    :param subject: 主题
    :param description: 描述
    :param meeting: 会议信息
    :param usercodes: 需要发送会议的usercode
    :param repeat_info: 重复信息
    """

    if EntryNotice.TYPE_MAIL in meeting.get("noticeType", ""):
        employees = meeting["employees"]
        if usercodes:
            employees = [
                employee for employee in employees if employee["usercode"] in usercodes
            ]

        owner = meeting["owner"]

        email_list = [employee["username"] + "@17zuoye.com" for employee in employees]
        email_json = {
            employee["username"] + "@17zuoye.com": employee["name"]
            for employee in employees
        }
        startTime = int(meeting["startTime"] / 1000)
        endTime = int(meeting["endTime"] / 1000)

        dtstart = timestamp_to_datetime_str(startTime, fmt="%Y%m%dT%H%M00")
        dtend = timestamp_to_datetime_str(endTime, fmt="%Y%m%dT%H%M00")
        email_json = json.dumps(email_json)

        description = description.replace("<div>", "")
        description = description.replace("</div>", "")
        if not repeat_info:
            _send_notice_mail.delay(
                subject=subject,
                description=description,
                email_list=",".join(email_list),
                email_json=email_json,
                uid=meeting["uid"],
                location=meeting["location"],
                method=method,
                sender=owner["username"],
                dtstart=dtstart,
                dtend=dtend,
            )
        else:
            rep_end_date = timestamp_to_datetime_str(
                repeat_info["rep_end_date"], fmt="%Y%m%dT%H%M00"
            )
            skip_list = [
                timestamp_to_datetime_str(v, fmt="%Y%m%dT%H%M00Z")
                for v in repeat_info["skip_list"]
            ]

            _send_notice_mail.delay(
                subject=subject,
                description=description,
                email_list=",".join(email_list),
                email_json=email_json,
                uid=meeting["uid"],
                location=meeting["location"],
                method=method,
                sender=owner["username"],
                dtstart=dtstart,
                dtend=dtend,
                skip_list=skip_list,
                rep_type=repeat_info["rep_type"],
                rep_num_weeks="1",
                rep_end_date=rep_end_date,
                month_absolute=repeat_info["month_absolute"],
                rep_day=repeat_info["rep_day"],
            )


@celery_app.task(bind=True)
def _send_notice_mail(self, interval=60, max_retries=3, **kw):
    """ 发送通知邮件"""
    try:
        MailService.send_canlendar_mail(**kw)
    except Exception:
        self.retry(countdown=interval, max_retries=max_retries)

@celery_app.task
def release_del_employee_meeting():
    """释放离职会议室"""
    deleted_employees = EmployeeService.get_deleted_employee_list()
    for item in deleted_employees:
        print item.usercode
        m = MeetingService.get_by_own(item.username)
        r = RepeatService.get_by_own(item.username)
        for item in m:
            MeetingService.cancel(item.id)
        for item in r:
            RepeatService.delete_by_id(item.id)
