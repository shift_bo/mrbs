# coding=utf-8
# flake8: noqa
import qrcode
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

from app import app


def qrcode_make(data, name, text_data):
    data = data
    text_data = text_data
    text_long = len(text_data)
    align = (250, 200)
    if text_long == 2:
        align = (400, 200)
    elif text_long == 3:
        align = (300, 200)
    elif text_long == 4:
        align = (280, 200)
    elif text_long == 5:
        align = (250, 200)
    img_file = r'../mrbs-web/src/assets/images/qrcode/'
    img_name = name
    qr = qrcode.QRCode(version=2)
    qr.add_data(data)
    qr.make(fit=True)
    img = qr.make_image()
    img = img.resize((800, 800))
    img_format = Image.open('scripts/format.png')
    img_w, img_h = img_format.size
    img_format.paste(img, (230, 630))
    draw = ImageDraw.Draw(img_format)
    ttfront = ImageFont.truetype('scripts/simsun.ttc', 180, encoding='gb2312')
    draw.text(align, text_data, "black", font=ttfront)
    img_format.save(img_file + img_name + '.png')
    # img.show()


@app.cli.command()
def static_qrcode():
    from models import db, Room
    URL = 'http://mrbs.17zuoye.net/#/meeting/new?roomId='
    rooms = Room.query.all()
    for i in rooms:
        url = URL + str(i.id)
        print i.room_name, url
        qrcode_make(url, i.room_name, i.room_name)
