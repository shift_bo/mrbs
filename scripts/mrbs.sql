use MeetingRoomBookingSystem_UsTalk;

CREATE TABLE IF NOT EXISTS `mrbs_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL UNIQUE,
  `display_name` varchar(20) NOT NULL default '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `mrbs_department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `parent_id` int(11) default 0,
  `order` int(11) default 0,
  `deleted` TINYINT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `idx_pid` (`parent_id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `mrbs_employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usercode` varchar(20) NOT NULL UNIQUE,
  `username` varchar(20) NOT NULL UNIQUE,
  `name` varchar(20) NOT NULL default '',
  `role_id` int(11) NOT NULL,
  `department_id` int(11) default 0,
  `email` varchar(75) default '',
  `avatar` varchar(255) default '',
  `deleted` TINYINT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `idx_usercode` (`usercode`),
  INDEX `idx_username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `mrbs_area_manager` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `area_id` int(11) NOT NULL,
  `usercode` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `mrbs_device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL UNIQUE,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `mrbs_room_device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `mrbs_entry_notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entry_id` int(11) NOT NULL UNIQUE,
  `type` varchar(10) default '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `mrbs_entry_employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entry_id` int(11) NOT NULL,
  `usercode` varchar(20) NOT NULL,
  `is_owner` TINYINT(1) default 0,
  `from_department` TINYINT(1) default 0,
  `status` int(11) default 0,
  `deleted` TINYINT(1) default 0,
  PRIMARY KEY (`id`),
  INDEX `idx_entry_id` (`entry_id`),
  INDEX `idx_usercode` (`usercode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `mrbs_entry_department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entry_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `deleted` TINYINT(1) default 0,
  PRIMARY KEY (`id`),
  INDEX `idx_entry_id` (`entry_id`),
  INDEX `idx_department_id` (`department_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
