# coding=utf-8
# flake8: noqa
import click
from flask_migrate import Migrate

from app import app
from extends import db
from models import Area
from models import AreaManager
from models import Device
from models import Employee
from models import Entry
from models import EntryEmployee
from models import EntryNotice
from models import Repeat
from models import Role
from models import Room
from models import RoomDevice
from models import User
from tasks.notice import release_del_employee_meeting
from service.employee import EmployeeService
# noqa: F401
import sys 
reload(sys) 
sys.setdefaultencoding('utf-8') 

migrate = Migrate(app, db)

@app.cli.command()
def sync_create_usercode():
    """同步会议创建人usercode"""
    for item in Entry.query.all():
        usercode = EmployeeService.get_usercode_by_create_name(item.create_by)
        item.create_by_usercode = usercode
        print usercode
    db.session.commit()
@app.cli.command()
def release_meeting():
    """释放离职会议室"""
    release_del_employee_meeting()

@app.cli.command()
def sync():
    """ 同步部门和员工"""
    from tasks.employee import sync_employee, sync_department

    sync_department()
    sync_employee()


@app.cli.command()
@click.option("--id")
def dep(id):
    """ 查询部门下的所有成员 flask dep --id <dep_id>
    """
    from service.ext.weixin import WXService
    members = WXService.get_department_member_detail(id)
    print(members)


@app.cli.command()
@click.option("--code")
def user(code):
    """ 查询用户信息 flask user --code <code>
    """
    from service.ext.weixin import WXService

    info = WXService.get_user_info(code)
    print(info)


@app.cli.command()
def clear():
    return
    for i in Entry.query.all():
        db.session.delete(i)

    for i in EntryNotice.query.all():
        db.session.delete(i)

    for i in EntryEmployee.query.all():
        db.session.delete(i)

    for i in Repeat.query.all():
        db.session.delete(i)

    db.session.commit()


@app.cli.command()
def init_db():
    """ 初始化数据库"""

    # 初始化角色
    role_name = [("ADMIN", "管理员"), ("MEMBER", "用户")]
    for name in role_name:
        role = Role(name=name[0], display_name=name[1])
        db.session.add(role)
    else:
        db.session.commit()

    # 初始化区域
    area_name = ["北京-绿地中心", "北京-浦项中心", "南京-新发展国际中心"]
    for name in area_name:
        area = Area(area_name=name)
        db.session.add(area)
    else:
        db.session.commit()

    area_room_map = {
        "北京-绿地中心": ["16-北京", "16-金星", "16-火星", "16-冥王星", "16-水星", "19-上海"],
        "北京-浦项中心": ["爱迪生", "牛顿", "苏格拉底", "达尔文"],
        "南京-新发展国际中心": ["北大", "清华", "港大"],
    }

    for k, v in area_room_map.items():
        area = Area.query.filter_by(area_name=k).first()
        for room_name in v:
            room = Room(area_id=area.id, room_name=room_name)
            db.session.add(room)
    else:
        db.session.commit()
