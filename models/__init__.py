# coding=utf-8
from datetime import datetime

from flask_login import UserMixin

from extends import db


class User(db.Model, UserMixin):
    """ 用户, PHP系统旧表"""

    __tablename__ = "mrbs_users"

    id = db.Column(db.Integer, primary_key=True)
    level = db.Column(db.Integer, nullable=False, default=0)
    name = db.Column(db.String(30), unique=True)
    password_hash = db.Column(db.String(255))
    email = db.Column(db.String(75))


class Role(db.Model):
    """ 角色"""

    __tablename__ = "mrbs_role"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), unique=True, nullable=False)
    display_name = db.Column(db.String(20), nullable=False)


class Department(db.Model):
    """ 部门信息

    :uid: unique_id， 企业微信中的部门id
    """

    __tablename__ = "mrbs_department"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    parent_id = db.Column(db.Integer, default=0)
    order = db.Column(db.Integer, default=0)
    deleted = db.Column(db.Boolean, default=False)


class Employee(db.Model):
    """ 员工信息"""

    __tablename__ = "mrbs_employee"

    id = db.Column(db.Integer, primary_key=True)
    usercode = db.Column(db.String(20), unique=True, nullable=False)
    username = db.Column(db.String(20), nullable=False)
    name = db.Column(db.String(20), nullable=False)
    role_id = db.Column(db.Integer, nullable=False)
    department_id = db.Column(db.Integer, default=0)
    email = db.Column(db.String(75))
    avatar = db.Column(db.String(255))
    deleted = db.Column(db.Boolean, default=False)


class AreaManager(db.Model):
    """ 区域管理员"""

    __tablename__ = "mrbs_area_manager"

    id = db.Column(db.Integer, primary_key=True)
    area_id = db.Column(db.Integer, nullable=False)
    usercode = db.Column(db.String(20), nullable=False)


class Area(db.Model):
    """ 区域 PHP系统旧表"""

    __tablename__ = "mrbs_area"

    ST_ENABLE = 0
    ST_DISABLE = 1

    id = db.Column(db.Integer, primary_key=True)
    disabled = db.Column(db.Integer, nullable=False, default=0)
    area_name = db.Column(db.String(30), unique=True)
    timezone = db.Column(db.String(50))
    area_admin_email = db.Column(db.Text)
    resolution = db.Column(db.Integer)
    default_duration = db.Column(db.Integer)
    default_duration_all_day = db.Column(db.Integer, nullable=False, default=0)
    morningstarts = db.Column(db.Integer)
    morningstarts_minutes = db.Column(db.Integer)
    eveningends = db.Column(db.Integer)
    eveningends_minutes = db.Column(db.Integer)
    private_enabled = db.Column(db.Integer)
    private_default = db.Column(db.Integer)
    private_mandatory = db.Column(db.Integer)
    private_override = db.Column(db.String(32))
    min_create_ahead_enabled = db.Column(db.Integer)
    min_create_ahead_secs = db.Column(db.Integer)
    max_create_ahead_enabled = db.Column(db.Integer)
    max_create_ahead_secs = db.Column(db.Integer)
    min_delete_ahead_enabled = db.Column(db.Integer)
    min_delete_ahead_secs = db.Column(db.Integer)
    max_delete_ahead_enabled = db.Column(db.Integer)
    max_delete_ahead_secs = db.Column(db.Integer)
    max_per_day_enabled = db.Column(db.Integer, nullable=False, default=0)
    max_per_day = db.Column(db.Integer, nullable=False, default=0)
    max_per_week_enabled = db.Column(db.Integer, nullable=False, default=0)
    max_per_week = db.Column(db.Integer, nullable=False, default=0)
    max_per_month_enabled = db.Column(db.Integer, nullable=False, default=0)
    max_per_month = db.Column(db.Integer, nullable=False, default=0)
    max_per_year_enabled = db.Column(db.Integer, nullable=False, default=0)
    max_per_year = db.Column(db.Integer, nullable=False, default=0)
    max_per_future_enabled = db.Column(db.Integer, nullable=False, default=0)
    max_per_future = db.Column(db.Integer, nullable=False, default=0)
    max_duration_enabled = db.Column(db.Integer, nullable=False, default=0)
    max_duration_secs = db.Column(db.Integer, nullable=False, default=0)
    max_duration_periods = db.Column(db.Integer, nullable=False, default=0)
    custom_html = db.Column(db.Text)
    approval_enabled = db.Column(db.Integer)
    reminders_enabled = db.Column(db.Integer)
    enable_periods = db.Column(db.Integer)
    confirmation_enabled = db.Column(db.Integer)
    confirmed_default = db.Column(db.Integer)


class Room(db.Model):
    """ 会议室, PHP系统旧表"""

    __tablename__ = "mrbs_room"

    ST_ENABLE = 0
    ST_DISABLE = 1

    id = db.Column(db.Integer, primary_key=True)
    disabled = db.Column(db.Integer, nullable=False, default=0)
    area_id = db.Column(db.Integer, nullable=False)
    room_name = db.Column(db.String(25), nullable=False, default="")
    sort_key = db.Column(db.String(25), nullable=False, default="")
    description = db.Column(db.String(60))
    capacity = db.Column(db.Integer, nullable=False, default=0)
    room_admin_email = db.Column(db.Text)
    custom_html = db.Column(db.Text)


class Device(db.Model):
    """ 会议室设备"""

    __tablename__ = "mrbs_device"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30), unique=True)


class RoomDevice(db.Model):
    """ 会议室-设备 关联表"""

    __tablename__ = "mrbs_room_device"

    id = db.Column(db.Integer, primary_key=True)
    room_id = db.Column(db.Integer, nullable=False)
    device_id = db.Column(db.Integer, nullable=False)


class Entry(db.Model):
    """ 会议登记, PHP系统旧表"""

    __tablename__ = "mrbs_entry"

    # status 0: 使用， 4：暂订

    id = db.Column(db.Integer, primary_key=True)
    start_time = db.Column(db.Integer, nullable=False, default=0)
    end_time = db.Column(db.Integer, nullable=False, default=0)
    entry_type = db.Column(db.Integer, nullable=False, default=0)
    repeat_id = db.Column(db.Integer)
    room_id = db.Column(db.Integer, nullable=False, default=1)
    timestamp = db.Column(db.TIMESTAMP, default=datetime.now)
    create_by = db.Column(db.String(80), nullable=False, default="")
    modified_by = db.Column(db.String(80), nullable=False, default="")
    name = db.Column(db.String(80), nullable=False, default="")
    type = db.Column(db.String(1), nullable=False, default="I")
    description = db.Column(db.Text)
    status = db.Column(db.Integer, nullable=False, default=0)
    reminded = db.Column(db.Integer)
    info_time = db.Column(db.Integer)
    info_user = db.Column(db.String(80))
    info_text = db.Column(db.Text)
    ical_uid = db.Column(db.String(255), nullable=False, default="")
    ical_sequence = db.Column(db.Integer, nullable=False, default=0)
    ical_recur_id = db.Column(db.String(16))
    email_list = db.Column(db.Text)
    # create_by_usercode = db.Column(db.String(80),nullable=False,default="")


class EntryNotice(db.Model):
    """ 会议通知"""

    TYPE_WX = "1"
    TYPE_MAIL = "2"

    __tablename__ = "mrbs_entry_notice"

    id = db.Column(db.Integer, primary_key=True)
    entry_id = db.Column(db.Integer, nullable=False, unique=True)
    type = db.Column(db.String(10), default="")


class EntryEmployee(db.Model):
    """ 会议登记的员工"""

    __tablename__ = "mrbs_entry_employee"

    ST_PENDING = 0  # 等待
    ST_ACCEPT = 1  # 接受
    ST_REST = 2  # 请假

    id = db.Column(db.Integer, primary_key=True)
    entry_id = db.Column(db.Integer, nullable=False)
    usercode = db.Column(db.String(20), nullable=False)
    is_owner = db.Column(db.Boolean, default=False)
    from_department = db.Column(db.Boolean, default=False)
    status = db.Column(db.Integer, default=ST_PENDING)
    deleted = db.Column(db.Boolean, default=False)


class EntryDepartment(db.Model):
    """ 会议登记的部门"""

    __tablename__ = "mrbs_entry_department"

    id = db.Column(db.Integer, primary_key=True)
    entry_id = db.Column(db.Integer, nullable=False)
    department_id = db.Column(db.Integer, nullable=False)
    deleted = db.Column(db.Boolean, default=False)


class Repeat(db.Model):
    """ 重复会议, PHP系统旧表"""

    __tablename__ = "mrbs_repeat"

    REP_NONE = 0
    REP_DAILY = 1
    REP_WEEKLY = 2
    REP_MONTHLY = 3

    id = db.Column(db.Integer, primary_key=True)
    start_time = db.Column(db.Integer, nullable=False, default=0)
    end_time = db.Column(db.Integer, nullable=False, default=0)
    rep_type = db.Column(db.Integer, nullable=False, default=0)
    end_date = db.Column(db.Integer, nullable=False, default=0)
    rep_opt = db.Column(db.String(32), nullable=False, default="")
    room_id = db.Column(db.Integer, nullable=False, default=1)
    timestamp = db.Column(db.TIMESTAMP, default=datetime.now)
    create_by = db.Column(db.String(80), nullable=False, default="")
    modified_by = db.Column(db.String(80), nullable=False, default="")
    name = db.Column(db.String(80), nullable=False, default="")
    type = db.Column(db.String(1), nullable=False, default="I")
    description = db.Column(db.Text)
    rep_num_weeks = db.Column(db.Integer)
    month_absolute = db.Column(db.Integer)
    month_relative = db.Column(db.Integer)
    status = db.Column(db.Integer, nullable=False, default=0)
    reminded = db.Column(db.Integer)
    info_time = db.Column(db.Integer)
    info_user = db.Column(db.String(80))
    info_text = db.Column(db.Text)
    ical_uid = db.Column(db.String(255), nullable=False, default="")
    ical_sequence = db.Column(db.Integer, nullable=False, default=0)
    email_list = db.Column(db.Text)
